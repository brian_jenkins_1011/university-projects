import sys, os


### Chess Pieces ###
valid_pieces = ['-', 'p', 'r', 'k', 'b', 'q', 'k', 'b', 'n', 'r', 'P', 'R', 'K', 'B', 'Q', 'K', 'B', 'N', 'R'] 

def get_score(piece_char):
	if (piece_char.upper() == "P"):
		return 1
	elif (piece_char.upper() == "R"):
		return 5
	elif (piece_char.upper() == "K"):
		return 3
	elif (piece_char.upper() == "B"):
		return 3
	elif (piece_char.upper() == "Q"):
		return 9
	else:
		return 200



def check_bound(pos):
	if (pos[0] >= 0 and pos[0] < 8) and (pos[1] >= 0 and pos[1] < 8):
		return True
	return False


def different_color(pos1, pos2, chess_matrix): #From, to, matrix
	pos1_x = pos1[0]
	pos1_y = pos1[1]
	pos2_x = pos2[0]
	pos2_y = pos2[1]

	if (chess_matrix[pos2_x][pos2_y] in valid_pieces):
		pos1_case = chess_matrix[pos1_x][pos1_y].isupper()
		pos2_case = chess_matrix[pos2_x][pos2_y].isupper()

		if (pos1_case != pos2_case):
			return True
		else:
			return False

	"""
		if (chess_matrix[pos1_x][pos1_y].isupper() and chess_matrix[pos2_x][pos2_y].islower()):
			return True
		elif (chess_matrix[pos1_x][pos1_y].islower() and chess_matrix[pos2_x][pos2_y].isupper()):
			return True
		return False
	return False
	"""


def get_vertical(pos, chess_matrix):
	moves = []

	#Down
	count = 1
	for i in range(len(chess_matrix)-1):
		if check_bound([pos[0]+count, pos[1]]):
			if (chess_matrix[pos[0]+count][pos[1]] != "-"):
				if (different_color(pos, [pos[0]+count, pos[1]], chess_matrix)):
					moves.append([pos[0]+count, pos[1]])
				break
			else:
				moves.append([pos[0]+count, pos[1]])
		else:
			break
		count+=1
	
	#Up 
	count = 1
	for i in range(len(chess_matrix)-1):
		if check_bound([pos[0]-count, pos[1]]):
			if (chess_matrix[pos[0]-count][pos[1]] != "-"):
				if (different_color(pos, [pos[0]-count, pos[1]], chess_matrix)):
					moves.append([pos[0]-count, pos[1]])
				break
			else:
				moves.append([pos[0]-count, pos[1]])
		else:
			break
		count+=1

	#Right
	count = 1
	for i in range(len(chess_matrix)-1):
		if check_bound([pos[0], pos[1]+count]):
			if (chess_matrix[pos[0]][pos[1]+count] != "-"):
				if (different_color(pos, [pos[0], pos[1]+count], chess_matrix)):
					moves.append([pos[0], pos[1]+count])
				break
			else:
				moves.append([pos[0], pos[1]+count])
		else:
			break
		count+=1

	#Left
	count = 1
	for i in range(len(chess_matrix)-1):
		if check_bound([pos[0], pos[1]-count]):
			if (chess_matrix[pos[0]][pos[1]-count] != "-"):
				if (different_color(pos, [pos[0], pos[1]-count], chess_matrix)):
					moves.append([pos[0], pos[1]-count])
				break
			else:
				moves.append([pos[0], pos[1]-count])
		else:
			break
		count+=1

	return moves



def get_diagonal(pos, chess_matrix):
	moves = [] 

	#Down Right
	count = 1
	for i in range(len(chess_matrix)-1):
		if (check_bound([pos[0]+count, pos[1]+count])):
			if (chess_matrix[pos[0]+count][pos[1]+count] != "-"):
				if (different_color(pos, [pos[0]+count, pos[1]+count], chess_matrix)):
					moves.append([pos[0]+count, pos[1]+count])
				break
			else:
				moves.append([pos[0]+count, pos[1]+count])
		else:
			break
		count+=1

	#Up Left
	count = 1
	for i in range(len(chess_matrix)-1):
		if (check_bound([pos[0]-count, pos[1]-count])):
			if (chess_matrix[pos[0]-count][pos[1]-count] != "-"):
				if (different_color(pos, [pos[0]-count, pos[1]-count], chess_matrix)):
					moves.append([pos[0]-count, pos[1]-count])
				break
			else:
				moves.append([pos[0]-count, pos[1]-count])
		else:
			break
		count+=1

	#Up Right
	count = 1
	for i in range(len(chess_matrix)-1):
		if (check_bound([pos[0]-count, pos[1]+count])):
			if (chess_matrix[pos[0]-count][pos[1]+count] != "-"):
				if (different_color(pos, [pos[0]-count, pos[1]+count], chess_matrix)):
					moves.append([pos[0]-count, pos[1]+count])
				break
			else:
				moves.append([pos[0]-count, pos[1]+count])
		else:
			break
		count+=1

	#Down Left
	count = 1
	for i in range(len(chess_matrix)-1):
		if (check_bound([pos[0]+count, pos[1]-count])):
			if (chess_matrix[pos[0]+count][pos[1]-count] != "-"):
				if (different_color(pos, [pos[0]+count, pos[1]-count], chess_matrix)):
					moves.append([pos[0]+count, pos[1]-count])
				break
			else:
				moves.append([pos[0]+count, pos[1]-count])
		else:
			break
		count+=1

	return moves



class pawn: 
	def __init__(self, color):
		self.name = "Pawn"
		self.has_moved = 0
		self.color = color
		self.val = 1

	def get_possible_moves(self, pos, chess_matrix): 
		moves = []
		
		if (self.color == 0):
			up = pos[0]-1
			down = pos[0]+1
		else:
			up = pos[0]+1
			down = pos[0]-1
		
		left = pos[1]-1
		right = pos[1]+1

		#TODO En Passant

		if (self.has_moved == 0): #Starting position
			if chess_matrix[up][pos[1]] == "-" and check_bound([up, pos[1]]):
				moves.append([up, pos[1]])
			
			if chess_matrix[up-1][pos[1]] == "-" and check_bound([up-1, pos[1]]):
				moves.append([up-1, pos[1]])
		
		else: #Forward movement
			if chess_matrix[up][pos[1]] == "-" and check_bound([up, pos[1]]):
				moves.append([up, pos[1]])

		
		#Diagonal Take (ineffiecnt but running out of time here)
		if (self.color == 0):
			if (check_bound([up, left])):
				if chess_matrix[up][left] != "-": #If diagonal left is not empty
					if (chess_matrix[up][left].isupper()): #Is a black piece
						moves.append([up,left])

			if (check_bound([up, right])):
				if chess_matrix[up][right] != "-": #If diagonal right is not empty
					if (chess_matrix[up][right].isupper()): #Is a black piece
						moves.append([up,right] ) 
		else:
			if (check_bound([up, left])):
				if chess_matrix[up][left] != "-": #If diagonal left is not empty
					if (chess_matrix[up][left].islower()): #Is a black piece
						moves.append([up,left])

			if (check_bound([up, right])):
				if chess_matrix[up][right] != "-": #If diagonal right is not empty
					if (chess_matrix[up][right].islower()): #Is a black piece
						moves.append([up,right] ) 

		return moves


class rook: 
	def __init__(self, color):
		self.name = "Rook"
		self.has_moved = 0
		self.color = color 
		self.val = 5

	def get_possible_moves(self, pos, chess_matrix):
		moves = get_vertical(pos, chess_matrix)
		
		#TODO: Castling
		
		return moves


class knight: 
	def __init__(self, color):
		self.name = "Knight"
		self.has_moved = 0
		self.color = color 
		self.val = 3

	def get_possible_moves(self, pos, chess_matrix):
		moves = []
		validated_moves = [] 

		moves.append([pos[0]+2, pos[1]+1])
		moves.append([pos[0]+2, pos[1]-1])
		moves.append([pos[0]-2, pos[1]+1])
		moves.append([pos[0]-2, pos[1]-1])

		moves.append([pos[0]+1, pos[1]-2])
		moves.append([pos[0]+1, pos[1]+2])
		moves.append([pos[0]-1, pos[1]-2])
		moves.append([pos[0]-1, pos[1]+2])
		
		for i in range(len(moves)):
			if (check_bound(moves[i])):
				pos_to = moves[i]
				if (chess_matrix[pos_to[0]][pos_to[1]] != "-"):
					if different_color(pos, pos_to, chess_matrix):
						validated_moves.append(moves[i])
				else:
					validated_moves.append(moves[i])

		return validated_moves


class bishop: 
	def __init__(self, color):
		self.name = "Bishop"
		self.has_moved = 0
		self.color = color 
		self.val = 3


	def get_possible_moves(self, pos, chess_matrix):
		moves = get_diagonal(pos, chess_matrix)
		return moves



class queen: 
	def __init__(self, color):
		self.name = "Queen"
		self.has_moved = 0
		self.color = color 
		self.val = 9


	def get_possible_moves(self, pos, chess_matrix):
		moves = get_diagonal(pos, chess_matrix)
		moves_verical = get_vertical(pos, chess_matrix)
		moves.extend(moves_verical) 
		return moves


class king: 
	def __init__(self, color):
		self.name = "King"
		self.has_moved = 0
		self.color = color 
		self.val = 200


	def get_possible_moves(self, pos, chess_matrix):
		moves = []
		validated_moves = [] 
		
		#TODO: Castling
		
		moves.append([pos[0]+1, pos[1]])
		moves.append([pos[0], pos[1]+1])
		moves.append([pos[0]-1, pos[1]])
		moves.append([pos[0], pos[1]-1])

		moves.append([pos[0]+1, pos[1]-1])
		moves.append([pos[0]+1, pos[1]+1])
		moves.append([pos[0]-1, pos[1]-1])
		moves.append([pos[0]-1, pos[1]+1])

		for i in range(len(moves)):
			if (check_bound(moves[i])):
				pos_to = moves[i]
				if (chess_matrix[pos_to[0]][pos_to[1]] != "-"):
					if different_color(pos, pos_to, chess_matrix):
						validated_moves.append(moves[i])
				else:
					validated_moves.append(moves[i])

		return validated_moves



