import sys
import os 
import piece
import math 
import copy
import csv

"""
This engine recieves a current chess board and determines which move to make against the player. For
simplicity, the board always assumes that its playing as black. 

"""
class ai_engine: 
	
	inf = 2**10000
	neg_inf = -2**10000

	black_moves = []
	white_moves = [] 
	original_matrix = [] 

	pawn_table = [] 
	rook_table = []
	knight_table = []
	bishop_table = []
	queen_table = [] 
	king_table = [] 

	white_score = 0
	black_score = 0


	def set_moves(self, black, white):
		self.black_moves = black
		self.white_moves = white


	def set_orig_board(self, matrix):
		self.original_matrix = matrix


	def get_scores(self, chess_matrix):
		self.white_score = 0
		self.black_score = 0

		for i in range(len(chess_matrix)): #Row
			for j in range(len(chess_matrix[i])): #Obj 
				
				if (chess_matrix[i][j] != "-"):
					if chess_matrix[i][j].islower():
						self.white_score+=piece.get_score(chess_matrix[i][j])
					else:
						self.black_score+=piece.get_score(chess_matrix[i][j])


	"""
	Collect piece square csv's for a less naive (but still naive) evaluation implementation
	Should be noted that these tables are rotated as we are dealing with black pieces, not white
	as the website shows.
	"""
	def collect_square_tables(self):
		self.pawn_table = list(csv.reader(open("piece_square_tables/pawn.csv")))
		self.rook_table = list(csv.reader(open("piece_square_tables/rook.csv")))
		self.knight_table = list(csv.reader(open("piece_square_tables/knight.csv")))
		self.bishop_table = list(csv.reader(open("piece_square_tables/bishop.csv")))
		self.queen_table = list(csv.reader(open("piece_square_tables/queen.csv")))
		self.king_table = list(csv.reader(open("piece_square_tables/king_middle.csv")))



	"""
	Naive approach uses standard evaluations for pieces outlined in:

	Capablanca, Jose; de Firmian, Nick (2006), 
	Chess Fundamentals (Completely Revised and Updated for the 21st century), 
	Random House, ISBN 0-8129-3681-7
	"""
	def evaluate_naive(self, color):
		if color == 0:
			return self.white_score - self.black_score
		else:
			return self.black_score - self.white_score


	"""
	Heuristic Evaluation function based off of: https://www.chessprogramming.org/Simplified_Evaluation_Function
	"""
	def evaluate(self, matrix, color):
		self.white_score = 0 
		self.black_score = 0 

		#Get Evaluation
		for i in range(len(matrix)):
			for j in range(len(matrix[i])):
				if matrix[i][j] != "-":
					cur = matrix[i][j]
					
					if cur == "p":
						self.white_score += int(self.pawn_table[i][j])
					elif cur == "P":
						self.black_score += int(self.pawn_table[i][j])

					elif cur == "r":
						self.white_score += int(self.rook_table[i][j])
					elif cur == "R":
						self.black_score += int(self.rook_table[i][j])

					elif cur == "n":
						self.white_score += int(self.knight_table[i][j])
					elif cur == "N":
						self.black_score += int(self.knight_table[i][j])

					elif cur == "b":
						self.white_score += int(self.bishop_table[i][j])
					elif cur == "B":
						self.black_score += int(self.bishop_table[i][j])

					elif cur == "q":
						self.white_score += int(self.queen_table[i][j])
					elif cur == "Q":
						self.black_score += int(self.queen_table[i][j])

					if cur == "k":
						self.white_score += int(self.king_table[i][j])
					elif cur == "K":
						self.black_score += int(self.king_table[i][j])

		
		#Return Evaluation
		if color == 0:
			return self.white_score - self.black_score
		else:
			return self.black_score - self.white_score

	
	"""
	Copy of make_move from board.py since I can't comprehend scope
	"""
	def move_piece(self, copy, move):
		move_from = move[0]
		move_to = move[1]

		if (copy[move_from[0]][move_from[1]] == "P") and (move_to[0] == 7):
			copy[move_to[0]][move_to[1]] = "Q" #Assume queen always
		else:
			copy[move_to[0]][move_to[1]] = copy[move_from[0]][move_from[1]]
		copy[move_from[0]][move_from[1]] = "-"
		
		return copy


	"""
	Minimax w Alpha-Beta Pruning

	Pseudocode taken from (https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning): 
	Russell, Stuart J.; Norvig, Peter (2003), 
	Artificial Intelligence: A Modern Approach (2nd ed.), 
	Upper Saddle River, New Jersey: Prentice Hall, ISBN 0-13-790395-2
	"""
	def minimax_ab(self, chess_matrix, depth, a, b, color):

		if depth == 0:
			return self.evaluate(chess_matrix, color)

		if (color == 0): #White Maximizing
			value = self.neg_inf
			for move in self.white_moves:

				#Make Move
				c = copy.deepcopy(self.original_matrix)
				c = self.move_piece(c, move)

				#Update Value
				value = max(value, self.minimax_ab(c, depth-1, a, b, 1))
				
				#Prune
				a = max(a, value)
				if (a >= b):
					break

			return value

		if (color == 1): #Black Maximizing
			value = self.inf
			for move in self.black_moves:

				#Make Move
				c = copy.deepcopy(self.original_matrix)
				c = self.move_piece(c, move)

				#Update Value
				value = min(value, self.minimax_ab(c, depth-1, a, b, 0))
				
				#Prune
				b = min(b, value)
				if (b <= a):
					break

			return value
		

