import piece
import ai_engine
import os 
import sys 
import csv
import math
import copy

### Chess Board ###

"""
Supports board and player movement. Keeps track of game piece configurations (objects and in standard chess array)

TODO:
	Castling 
	En Passant
	Check(mate)
	Stalemate
"""

class chess_board:
	def __init__(self):
		#Vals
		self.pieces = ['-', 'p', 'r', 'k', 'b', 'q', 'k', 'b', 'n', 'r', 'P', 'R', 'K', 'B', 'Q', 'K', 'B', 'N', 'R'] 
		self.x_positions = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
		self.y_positions = [8,7,6,5,4,3,2,1]
		self.valid_colors = ["White", "Black"]

		self.chess_matrix = [] 
		self.chess_objects = []

		self.ai_engine = ai_engine.ai_engine() 
		self.ai_engine.collect_square_tables()
		self.depth = 3
		
		#Vals
		self.pieces = ['-', 'p', 'r', 'k', 'b', 'q', 'k', 'b', 'n', 'r', 'P', 'R', 'K', 'B', 'Q', 'K', 'B', 'N', 'R'] 
		self.x_positions = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
		self.y_positions = [8,7,6,5,4,3,2,1]
		self.valid_colors = ["White", "Black"]


	def set_depth(self, depth):
		self.depth = int(depth)

	def draw_board(self): #Print non-pythonically
		row_count = 8
		print("    A B C D E F G H\n")
		for i in range(len(self.chess_matrix)):
			print("{}   ".format(row_count), end="")
			
			for j in range(len(self.chess_matrix[0])):
				print("{} ".format(self.chess_matrix[i][j]), end="")
			
			#print("   {}\n".format(row_count), end="")
			print("   {}\n".format(i), end="")
			row_count-=1
		print()
		print("    A B C D E F G H\n")


	def print_objects(self):
		for i in range(len(self.chess_objects)):
			for j in range(len(self.chess_objects[i])):
				if self.chess_objects[i][j] is not None:
					print(self.chess_objects[i][j].name, end="")
				else:
					print("-", end="")
			print()


	def update_board(self): #Udpates object list(array)
		for i in range(len(self.chess_matrix)):
			self.chess_objects.append([None for i in range(8)])
			for j in range(len(self.chess_matrix[i])):
				
				#Pawn
				if (self.chess_matrix[i][j] == "P"):
					p = piece.pawn(1)
				elif (self.chess_matrix[i][j] == "p"):
					p = piece.pawn(0)
				
				#Rook
				elif (self.chess_matrix[i][j] == "R"):
					p = piece.rook(1)
				elif (self.chess_matrix[i][j] == "r"):
					p = piece.rook(0)

				#Knight
				elif (self.chess_matrix[i][j] == "N"):
					p = piece.knight(1)
				elif (self.chess_matrix[i][j] == "n"):
					p = piece.knight(0)

				#Bishop
				elif (self.chess_matrix[i][j] == "B"):
					p = piece.bishop(1)
				elif (self.chess_matrix[i][j] == "b"):
					p = piece.bishop(0)

				#Queen
				elif (self.chess_matrix[i][j] == "Q"):
					p = piece.queen(1)
				elif (self.chess_matrix[i][j] == "q"):
					p = piece.queen(0)
			
				#King
				elif (self.chess_matrix[i][j] == "K"):
					p = piece.king(1)
				elif (self.chess_matrix[i][j] == "k"):
					p = piece.king(0)			
				
				else:
					p = None

				self.piece_has_moved(p, [i,j])
				self.chess_objects[i][j] = p
				#self.print_objects()
		

		#Game Over
		white_king_exists = False
		black_king_exists = False
		for i in range(len(self.chess_matrix)):
			for j in range(len(self.chess_matrix[i])):
				if self.chess_matrix[i][j] == "K":
					black_king_exists = True
				elif self.chess_matrix[i][j]:
					white_king_exists = True

		if not white_king_exists:
			print("Game over! Black Wins!")
			sys.exit()
		elif not black_king_exists:
			print("Game over! White Wins!")
			sys.exit()



	

	#determine if a piece has moved for castling and en passant
	def piece_has_moved(self, piece, pos):
		if piece is not None:
			if piece.name == "Pawn": 
				if pos[0] < 6 and piece.color == 0:
					piece.has_moved = 1
				elif pos[0] > 1 and piece.color == 1:
					piece.has_moved = 1

			elif piece.name == "Rook": 
				if (pos[0] < 7) and (pos[1] != 0 or pos[1] != 7) and piece.color == 0:  
					piece.has_moved = 1
				elif (pos[0] > 0) and (pos[1] != 0 or pos[1] != 7) and piece.color == 1:  
					piece.has_moved = 1

			elif piece.name == "Knight": 
				if (pos[0] < 7) and (pos[1] != 1 or pos[1] != 6) and piece.color == 0:  
					piece.has_moved = 1
				elif (pos[0] > 0) and (pos[1] != 1 or pos[1] != 6) and piece.color == 1:  
					piece.has_moved = 1

			elif piece.name == "Bishop": 
				if (pos[0] < 7) and (pos[1] != 2 or pos[1] != 5) and piece.color == 0:  
					piece.has_moved = 1
				elif (pos[0] > 0) and (pos[1] != 2 or pos[1] != 5) and piece.color == 1:  
					piece.has_moved = 1

			elif piece.name == "Queen": 
				if (pos[0] < 7) and (pos[1] != 3) and piece.color == 0:  
					piece.has_moved = 1
				elif (pos[0] > 0) and (pos[1] != 3) and piece.color == 1:  
					piece.has_moved = 1

			else: #King
				if (pos[0] < 7) and (pos[1] != 4) and piece.color == 0:  
					piece.has_moved = 1
				elif (pos[0] > 0) and (pos[1] != 4) and piece.color == 1:  
					piece.has_moved = 1



	# Load user board configuration
	def set_configuration(self, file):
		#file = "start.csv"
		tmp_matrix = []
		
		#Read document
		self.chess_matrix = list(csv.reader(open(file)))

		#Verify Pieces
		for i in range(len(self.chess_matrix)):
			for j in range(len(self.chess_matrix[0])):
				if self.chess_matrix[i][j] not in self.pieces:
					print("ERROR - Invalid characters detected in CSV file: {}\nEXITING".format(self.chess_matrix[i][j]))
					exit()

		#Update Objects List
		self.update_board()


	#Verify user square selection has a piece on it
	def verify_square_selection(self, selection):
		try:
			if len(selection) == 2:
				if (int(selection[1]) > 0 and int(selection[1]) <=8) and (selection[0].upper() in self.x_positions):
					x = self.x_positions.index(selection[0].upper()) 
					y = self.y_positions.index(int(selection[1]))

					if (self.chess_matrix[y][x] != "-") and self.chess_objects[y][x].color == 0:
						return True
			return False
		except: #Some weird input? 
			return False

	#Verify user move is supported by currently selected square, piece
	def verify_move_selection(self, selection, moves):
		try:
			if len(selection) == 2:
				if (int(selection[1]) > 0 and int(selection[1]) <=8) and (selection[0].upper() in self.x_positions):
					x = self.x_positions.index(selection[0].upper()) 
					y = self.y_positions.index(int(selection[1]))

					if ([y,x] in moves):
						return True
			return False
		except: 
			return False

	
	
	#Return all possible moves for white
	def get_white_moves(self, chess_matrix):
		white_moves = [] 
		for i in range(len(self.chess_matrix)):
			for j in range(len(self.chess_matrix[i])):
				if (self.chess_matrix[i][j] != "-" and self.chess_matrix[i][j].islower()):
					piece_type = self.chess_objects[i][j]

					for move in piece_type.get_possible_moves([i,j], self.chess_matrix):
						white_moves.append([[i,j],move])

		return white_moves


	#Return all possible moves for black	
	def get_black_moves(self, chess_matrix):
		black_moves = []
		for i in range(len(self.chess_matrix)):
			for j in range(len(self.chess_matrix[i])):
				if (self.chess_matrix[i][j] != "-" and self.chess_matrix[i][j].isupper()):
					piece_type = self.chess_objects[i][j]

					for move in piece_type.get_possible_moves([i,j], self.chess_matrix):
						black_moves.append([[i,j],move])

		return black_moves



	#Return possible moves for selected piece
	def get_possible_moves(self, selection): #TODO - very expensive currently
		x = self.x_positions.index(selection[0].upper()) 
		y = self.y_positions.index(int(selection[1]))
		piece = self.chess_objects[y][x]
		
		piece_moves = piece.get_possible_moves([y,x], self.chess_matrix)

		return piece_moves, piece


	"""
	def stalemate_checkmate_check(self, board, color):
		king_in_check = False
		
		if color == 0:
			black_moves = self.get_black_moves(matrix_copy)
			for move in black_moves:
				move_to = move[1]

				if (copy[move_to[0]][move_to[1]] == "k"): #White in check
					king_in_check = True
					break 

			if king_in_check:


		else:
			white_moves = self.get_white_moves(matrix_copy)
			for move in white_moves:
				move_to = move[1]

				if (copy[move_to[0]][move_to[1]] == "K"): #Black in check
					king_in_check = True
					break 
	"""


	#Make a user made move 
	def make_move(self, fro, to):
		x_from = self.x_positions.index(fro[0].upper()) 
		y_from = self.y_positions.index(int(fro[1]))

		x_to = self.x_positions.index(to[0].upper()) 
		y_to = self.y_positions.index(int(to[1]))

		#Promotion
		if (self.chess_objects[y_from][x_from].name == "Pawn") and (y_to == 0):
			promotion = input("Promote pawn to (b,n,r,q): ")
			promotion = promotion.upper()

			if (promotion == "B"):
				self.chess_matrix[y_to][x_to] = "b"
			elif (promotion == "N"):
				self.chess_matrix[y_to][x_to] = "n"
			elif (promotion == "R"):
				self.chess_matrix[y_to][x_to] = "r"
			else: #Defaut to queen to prevent breaking
				self.chess_matrix[y_to][x_to] = "q"

		#Not Promotion
		else:
			self.chess_matrix[y_to][x_to] = self.chess_matrix[y_from][x_from]
		
		#Update Matrix
		self.chess_matrix[y_from][x_from] = "-"
		self.update_board()



	"""
	AI functionality powered by ai_engine.py
	"""
	def make_move_ai(self):
		inf = 2**10000
		neg_inf = -2**10000

		#collect all possible moves for both sides
		black_moves = self.get_black_moves(self.chess_matrix)

		#start searching for best black move
		best_move = [] 
		best_score = inf

		for i in range(len(black_moves)):

			#init the ai_engine to compute the best move for black
			matrix_copy = copy.deepcopy(self.chess_matrix)
			matrix_copy = self.ai_engine.move_piece(matrix_copy, black_moves[i])
			
			next_white_moves = self.get_white_moves(matrix_copy)
			next_black_moves = self.get_black_moves(matrix_copy)

			self.ai_engine.set_moves(next_black_moves, next_white_moves)
			self.ai_engine.set_orig_board(matrix_copy)

			score = self.ai_engine.minimax_ab(matrix_copy, self.depth, neg_inf, inf, 1)

			if (score < best_score):
				best_score = score
				best_move = black_moves[i]

		
		#update
		self.ai_engine.move_piece(self.chess_matrix, best_move)
		self.update_board()





		