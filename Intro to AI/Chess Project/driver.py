import sys
import csv
import os
import board
import piece


### Driver Code ####

#Get Player Input
def select_tile(cb):
	tile = input("Select a sqaure (b8): ")

	#User Base Cases
	if (tile == "c" or tile == "C"):
		return

	if (tile == "q" or tile == "Q"):
		print("See ya!")
		sys.exit()


	#Verify Selection
	if (cb.verify_square_selection(tile)):
		move_piece(cb, tile)
	
	else:
		print("Incorrect selection...")
		select_tile(cb)


#Move Selected Piece
def move_piece(cb, tile):
	moves, piece = cb.get_possible_moves(tile) 

	if len(moves) > 0:
		to_tile = input("Move {} {} to: ".format(cb.valid_colors[piece.color], piece.name))
		
		#User Base Cases
		if (to_tile == "c" or to_tile == "C"):
			return

		if (to_tile == "q" or to_tile == "Q"):
			sys.exit()

		#Verify Move
		if cb.verify_move_selection(to_tile, moves):
			cb.make_move(tile, to_tile) #Move Piece
			cb.make_move_ai()
			cb.draw_board() #Update Board
		else:
			print("Move not possible...")
			move_piece(cb, tile)
	
	else:
		print("No possible moves exist...")



def __main__():
	
	#Get Config (if selected)
	file = "" 
	try :
	    file = sys.argv[1]
	except:
	    file = "board_configs/start.csv"


	print("\nWelcome! You're playing as white. Have Fun!")
	print("Tip: Use 'c' to cancel current selection. Use 'q' at any time to quit.\n")
	
	#Collect User Parameters
	depth = input("Select a search depth (Anything > 5 is very slow): ")
	print()
	
	#Init Board
	cb = board.chess_board()
	cb.set_configuration(file)
	cb.set_depth(depth)
	cb.draw_board()


	running = True
	while (running):
		select_tile(cb)
	


__main__()