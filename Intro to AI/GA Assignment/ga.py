import os 
import sys 
import random 
import copy
import functools
import string
import itertools
import threading
import time
import math 
from datetime import datetime

#-- PRINTING + HOUSEKEEPING--#
def print_file(shredded):
	for i in shredded:
		for j in i:
			print(j, end=" ")
		print()


def print_unshred(unshredded):
	for i in range(len(unshredded[0])):
		for j in range (len(unshredded)):
			print(unshredded[j][i], end="", flush=True)
	print()



#--DECIPHERING --#
def collect_bigrams():
	file = "english_bigrams.txt" 
	bigrams = {} 

	if (os.path.exists(file)):
		with open (file, 'r') as f:
			eng_bi = f.read().splitlines()
	for line in eng_bi:
		bigrams[line[:2]] = float(line[3:])

	return bigrams


def unshred(shredded, perm):
	unshredded = [] 
	for i in range (len(shredded)):
		unshredded.append(shredded[perm[i]])
	
	print_unshred(unshredded)
	return unshredded 


"""
These fitness methods is simply a translation from the code Cody provided in tutorial. Fitness calculation proves
to match correcty (see example sol in main)
"""
def process_char(cur):
	ret = 0
	if (cur.isupper()):
		return string.ascii_uppercase.index(str(cur))
	elif (cur.islower()):
		return string.ascii_lowercase.index(str(cur))
	else:
		return -1
	return ret 

#Direct translation of provided fitnessCalculator.java fitness function 
def get_fitness(bigrams_list, shreds, perm):
	character_count = [[[0 for k in range(26)] for j in range(26)] for i in range(len(shreds[0]))]
	total_character = [0 for k in range(len(shreds[0]))] 

	for i in range(len(shreds[0])):
		for j in range (1, len(shreds)):
		
			back = process_char(shreds[perm[j]][i])
			cur = process_char(shreds[perm[j-1]][i])

			#print('{} {} {}'.format(i, back,cur))

			if cur != -1 and back != -1:
				total_character[i] += 1
				character_count[i][cur][back] += 1

	
	score = [0 for i in range(len(shreds[0]))]
	for line in range (len(shreds[0])):
		for i in range(26):
			for j in range(26):
				combo = str(chr(ord('A')+i)) + str(chr(ord('A')+j))
				expected = bigrams_list[combo]
				
				if (total_character[line] != 0):
					calculated = abs((character_count[line][i][j]/total_character[line])-expected)*(1.0-expected);
					score[line] += calculated	

	s = 0 
	for i in range(len(score)):
		s += pow(score[i],2)

	return s



#-- GA METHODS --#
def reciprocal_mutate(perm, chance):
	if random.random() < float(chance):
		a,b = random.sample(range(15),2)
		tmp = copy.deepcopy(perm)
		perm[a] = tmp[b] 
		perm[b] = tmp[a] 
		return perm 
	return perm 


def order_crossover(parent1, parent2, chance):
	if random.random() < float(chance):
		
		start, end = random.sample(range(15),2) 
		tmp1, tmp2 = copy.deepcopy(parent1), copy.deepcopy(parent2)

		if (start > end):
			start,end = end, start 

		def cross(p, t):
			child = [-1 for i in range(15)]
			child[start:end+1] = p[start:end+1]

			remain = [] 
			for i in range(len(p)):
				if (t[i] not in child):
					remain.append(t[i])

			for i in range (len(child)):
				if child[i] == -1:
					child[i] = remain[0]
					remain.pop(0)
			return child
		return cross(parent1, tmp2), cross(parent2, tmp1) 
	return parent1, parent2


def partially_mapped_crossover(parent1 ,parent2, chance):
	if random.random() < float(chance):
		start, end = random.sample(range(15),2)
		tmp1, tmp2 = copy.deepcopy(parent1), copy.deepcopy(parent2)

		if (start > end):
			start,end = end, start

		child1 = [-1 for i in range(15)]
		child2 = [-1 for i in range(15)]

		child1[start:end+1] = parent2[start:end+1]
		child2[start:end+1] = parent1[start:end+1]

		def cross(c, p1, p2):
			for i in range(len(c)):
				if (c[i] == -1) and (p1[i] not in c):
					c[i] = p1[i]

			for j in range(len(c)):
				if (c[j] == -1):
					for k in range(len(p2)):
						if (p2[k] not in c):
							c[j] = p2[k]
							break
			return c
		return cross(child1, parent1, parent2), cross(child2, parent2, parent1)
	return parent1, parent2


def crossover_helper(t, parent1, parent2, cx):
	if (int(t) == 0):
		return order_crossover(parent1, parent2, cx)
	elif (int(t) == 1):
		return partially_mapped_crossover(parent1, parent2, cx)

#For experimenting, not used in GA
def order_pop(P, bigrams, shredded):
	p = [] 
	ordered_P = [] 
	for i in range(len(P)):
		score = get_fitness(bigrams, shredded, P[i])
		p.append([P[i], score])

	p = sorted(p,key=lambda l:l[1], reverse=False)

	for i in range(len(p)):
		ordered_P.append(p[i][0])

	return ordered_P 


def populate(population_size):
	P = [] 
	while (len(P) < int(population_size)):
		perm = random.sample(range(15), 15)
		P.append(perm) 

	return P 


def tournament(P, k, shredded, bigrams_list):
	best_index = 0;
	best_fitness = 0; 

	random.shuffle(P) #Shuffle pop to get random K parents
	for i in range(k+1): 
		fitness = get_fitness(bigrams_list, shredded, P[i])
		if fitness < best_fitness:
			best_fitness = fitness
			best_index = i 

	return P[best_index] 


def find_best(pop, bigrams, shredded):
	best_perm = pop[0]
	best_score = get_fitness(bigrams, shredded, pop[0])

	for perm in pop:
		score = get_fitness(bigrams, shredded, perm)
		if (score < best_score):
			best_score = score
			best_perm = perm 
	
	return best_perm, best_score


def find_worst(pop, bigrams, shredded):
	worst_perm = pop[0]
	worst_score = get_fitness(bigrams, shredded, pop[0])

	for perm in pop:
		score = get_fitness(bigrams, shredded, perm)
		if (score > worst_score):
			worst_score = score
			worst_perm = perm 
	
	return worst_perm, worst_score


def elite_population(P, C, bigrams, shredded):
	best_c = [] 

	for i in range(5):
		best_perm, best_score = find_best(C, bigrams, shredded)
		best_c.append(best_perm)
		C.remove(best_perm) 
	
	for j in range(5):
		worst_perm, worst_score = find_worst(P, bigrams, shredded)
		P.remove(worst_perm)

	for k in range(len(best_c)):
		P.append(best_c[k]) 

	return P 


#-- BIG BOI --#
def main():
	seed = random.randrange(sys.maxsize)
	random.seed(seed)

	#Load In Files
	file = sys.argv[1]
	if (os.path.exists(file)):
		with open (file, 'r') as f:
			shredded = f.read().splitlines()
	print ("Shredded file imported!")

	bigrams = collect_bigrams()
	print("Bigrams imported!\n")

	#Example of doc1 solution fitness test 
	#sol = [2,0,7,12,4,1,11,5,14,3,8,13,10,6,9]	
	#print(get_fitness(bigrams, shredded, sol))

	#Get params
	cx = input("Set crossover rate (0.9 for 90%, 1 for 100%): ")
	cx_type = input("Set crossover implementation (0 - OX, 1 - PMX): ")
	mt = input ("Set mutation probability (0 for 0%, .10 for 10%): ")

	def k_check():
		kk = input ("Set number of competing parents for Tournament Selection (K = 1..5): ")
		k = int(kk) 
		if k < 1 or k > 5:
			print("K can be 1,2,3,4 or 5. Try again")
			k_check()
		return k 
	
	k = k_check()
	population_size = input ("Set population size (Min K+1): ")
	generation_span = input("Set generation span: ")

	#Start GA Process
	P = populate(population_size)
	best_fit_perm = [] 
	P_best_score = get_fitness(bigrams, shredded, P[0])
	iteration = 0 

	while (iteration < int(generation_span)-1):
		#Vars
		C = []
		P_score = 0
		iteration += 1


		#Generate New Population
		while len(C) < int(population_size):
			parent1 = tournament(P, k, shredded, bigrams)
			parent2 = tournament(P, k, shredded, bigrams)
			
			child1, child2 = crossover_helper(cx_type, parent1, parent2, cx)
			
			child1 = reciprocal_mutate(child1, mt)
			child2 = reciprocal_mutate(child2, mt)

			if not functools.reduce(lambda i, j : i and j, map(lambda m, k: m == k, child1, child2), True):
				fit1 = get_fitness(bigrams, shredded, child1)
				fit2 = get_fitness(bigrams, shredded, child2)

				#Only Include Best Child in New Population
				if fit1 <= fit2:
					C.append(child1)
				else:
					C.append(child2)

		# GA Info
		for perm in P:
			P_score += get_fitness(bigrams, shredded, perm)

		best_perm, best_score = find_best(P, bigrams, shredded)
		print('\nGen: {}\nBest Fitness: {}\nAverage Population Fitness: {}\n'.format(iteration, best_score, P_score/float(population_size)))

		#Take 5 best chromosones in P, remove worst 5 (this adds a lot of time, uncomment P=C for quicker runtime)
		P = elite_population(P, C, bigrams, shredded)
		#P = C 

	# GA Complete
	done = True

	for perm in P:
			P_score += get_fitness(bigrams, shredded, perm)

	best_perm, best_score = find_best(P, bigrams, shredded)
	print('Run Complete! (Gen {})'.format(iteration+1))
	print('Seed: {}\nBest Fitness: {}\nAverage Population Fitness: {}\nBest Chromosone: {}\n'.format(seed, best_score, P_Score, best_perm))
	print("Unshredded Document: ")
	unshred(shredded, best_perm)

main() 