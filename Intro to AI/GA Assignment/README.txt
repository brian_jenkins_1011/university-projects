Hi! 

First things first, I used python 3.6 for this assignment so in sandcastle (although I'm sure you already know) you'll have to run:

scl enable rh-python36 bash


Next, ensure that english_bigrams.txt is included in whichever dir you're running a2.py from. My script takes this file and reads the values into a dictionary for ease of access. 


Once these two are satisfied you can run the program by running (for example):

python a2.py document1-shredded.txt


You'll be prompted to input a crossover rate, crossover implementation, mutation rate, population size, generation size and K value for tournament selection. Then just sit back and watch the program do all the work! 


For a start, here are the values I typically used:

Set crossover rate (0.9 for 90%, 1 for 100%): 1
Set crossover implementation (0 - OX, 1 - PMX): 0 
Set mutation probability (0 for 0%, .10 for 10%): 0
Set population size: 50
Set generation span: 5
Set number of competing parents for Tournament Selection (K = 1..5): 2


Thanks!



--- Unnecessary Info ---

Included in the directory I handed in there are two files:
	cody.txt
	get_bigram_values.py 

To get my fitness calculator to work in python, I took the bigram values generated in the FitnessCalculator.java file provided in tutorial. Using the python script, I took substrings of each line to get the key and values before throwing them into english_bigrams.txt. 