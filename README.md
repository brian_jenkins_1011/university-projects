# Completed Course Work
The following projects and papers are what I feel to be the most relevant learning experiences during my time at University. Alongside the topics these items cover I was, and continue to be, incredibly interested in low-level systems, computer networks, and algorithms. 

## Papers and Major Projects
#### Machine Learning and Neural Networks
[Python - Sarcasm Detection in News Headlines Using Convolutional Neural Networks](https://gitlab.com/brian_jenkins_1011/university-projects/-/tree/main/Neural%20Networks/CNN%20Sarcasm%20Detection)

[Python - Classification of Thermopile Arrays Using a Feed Forward Neural Network](https://gitlab.com/brian_jenkins_1011/university-projects/-/tree/main/Neural%20Networks/Thermopile%20Classification)

[Python - Chess Simulator](https://gitlab.com/brian_jenkins_1011/university-projects/-/tree/main/Intro%20to%20AI/Chess%20Project)

[Python - Genetic Algorithm](https://gitlab.com/brian_jenkins_1011/university-projects/-/tree/main/Intro%20to%20AI/GA%20Assignment)

#### Undergraduate Project - Distributed Systems (4 Month)
[C++ - Virtual Resource Unit Assembly and Clustering a Distributed Network](https://gitlab.com/brian_jenkins_1011/university-projects/-/tree/main/Undergrad%20Project)

#### Mobile Applications
[Java - What Movie Today?](https://gitlab.com/brian_jenkins_1011/university-projects/-/tree/main/Mobile%20Computing/Final%20Project)


## Disclaimer
*I acknowledge that all posted projects and papers are the intellectual property of Brock University. This repository is intended for EDUCATIONAL purposes only and any documents which may be in conflict with Canadian copyright and/or IP law will be removed upon request.*
