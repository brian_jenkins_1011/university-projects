#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np 
import math
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
import torch.nn.functional as F
import random
import csv
import time
import datetime
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, multilabel_confusion_matrix
from sklearn.metrics import plot_confusion_matrix

from torchtext.legacy import data
from torchtext.legacy import datasets

vocab_size = 0 
padd_idx = 0

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# print('Device: ' + str(device))

class CNN(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, out_channels, filters, output_dim, dropout, pad_idx):
        
        super().__init__()
        
        #build the embedding layer (<pad> is used to pad sentences to the max length)
        self.embedding = nn.Embedding(num_embeddings, embedding_dim, padding_idx = pad_idx)
        
        #build the convolutional layers based off the given filters
        self.convs = nn.ModuleList([nn.Conv1d(in_channels = embedding_dim, 
                                              out_channels = out_channels, 
                                              kernel_size = fs)
                                    for i, fs in enumerate(filters)
                                    ])
        
        #build the fully connected layer for classifcation
        self.fc = nn.Linear(len(filters) * out_channels, output_dim)

        #set the dropout 
        self.dropout = nn.Dropout(dropout)

    #handles one forward pass through the network  
    def forward(self, headline):
        x = self.embedding(headline)

        x = x.permute(0, 2, 1)

        conv_layers = [F.relu(conv(x)) for conv in self.convs]
        pooling = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conv_layers]
        
        drop = self.dropout(torch.cat(pooling, dim = 1))
        return self.fc(drop)


#returns data iterators for the training and testing sets
def data_preprocess(seed, train_test_split, vector_idx):
    
    global vocab_size
    global padd_idx

    #Collect Unique Words
    df = pd.read_json('data/Sarcasm_Headlines_Dataset_v2.json', lines=True)

    unique_words = []
    headlines = []
    max_len = 0
    for i in range(len(df)):
        head = df.loc[i, 'headline']
        arr = head.split(" ")
        headlines.append(arr)

        if (len(arr) > max_len):
            max_len = len(arr)

        for j in range(len(arr)):
            unique_words.append(arr[j])

    unique_words = set(unique_words)
    
    #Collect JSON
    sentence_split = data.get_tokenizer("basic_english")

    is_sarcastic = data.LabelField(dtype=torch.float) 
    headline = data.Field(tokenize=sentence_split, fix_length=max_len, batch_first = True) 

    fields = {
            'is_sarcastic': ('s', is_sarcastic), 
            'headline': ('h', headline), 
    }

    train, test = data.TabularDataset(
                                path = 'data/Sarcasm_Headlines_Dataset_v2.json',
                                format = 'json',
                                fields = fields
    ).split(train_test_split, random_state = random.seed(seed)) 
    
    if(seed == 0):
        # print("Unique Words: {}".format(len(unique_words))) 
        # print("Max Sen Len: {}".format(max_len))
        print("Training set sentences: {}".format(len(train)))
        print("Testing set sentences: {}".format(len(test)))
    
    if(vector_idx == 0):
        vectors = None
        unk_init = None
    elif(vector_idx == 1):
        vectors = "charngram.100d" # 946mb
        unk_init = torch.Tensor.normal_
    elif(vector_idx == 2):
        vectors = "glove.6B.100d"
        unk_init = torch.Tensor.normal_
    elif(vector_idx == 3):
        vectors = "glove.twitter.27B.100d"
        unk_init = torch.Tensor.normal_

    headline.build_vocab(
        train,
        max_size = len(headlines),
        vectors = vectors, 
        unk_init = unk_init 
    )

    vocab_size = len(headline.vocab)

    is_sarcastic.build_vocab(train)

    padd_idx = headline.vocab.stoi[headline.pad_token]
    
    train_iterator, test_iterator = data.Iterator.splits(
        (train, test),
        sort_key=lambda x: len(x.h), #because you cant sort <Example> v <Example>
        sort_within_batch=False,
        batch_sizes = (64, 1)
    )

    return train_iterator, test_iterator, headline



classes = ('Non-Sarcastic', 'Sarcastic')
predicted_tensors = []
actual_tensors = []

# Returns 1 if the prediction was correct, else 0
def get_accuracy(pred, actual):
    pred = torch.round(torch.sigmoid(pred))
    correct = (pred == actual).float()
    p = np.squeeze(pred.cpu().numpy()) 
    
    predicted_tensors.append(classes[int(pred[0].cpu())])
    actual_tensors.append(classes[int(actual[0].item())])
    
    return correct.sum() / len(correct)

#Display Contingency Table
def display_contigency(truth, p, labels, title):
    contigency = confusion_matrix(truth, p, labels)

    plt.imshow(contigency, interpolation='nearest', cmap=plt.cm.Greens)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(labels))
    plt.xticks(tick_marks, labels, rotation=45)
    plt.yticks(tick_marks, labels)

    arr = np.asarray(contigency)
    fmt = '.2f'
    thresh = contigency / 2

    for i in range(contigency.shape[0]):
        for j in range(contigency.shape[1]):

            if (contigency[i][j] < thresh).any():
                color = "black"
            else:
                color = "white"

            plt.text(j, i, format(int(contigency[i][j]/30), fmt), color=color, horizontalalignment="center")

    plt.tight_layout()
    plt.ylabel('Truth')
    plt.xlabel('Predicted')

    plt.show()


def main(num_runs, epochs, learning_rate, train_test_split, vector_idx, optim_idx):
    acc_per_run = []
    total_train_time = 0
    loss_per_run = []
    
    for seed in range(0, num_runs):
        
        #get train and test data iterators
        train_iterator, test_iterator, headline = data_preprocess(seed, train_test_split, vector_idx)

        print("\nRun: {}/{}".format(seed + 1, num_runs))
        
        #init params: input_dim, embed_dim, num_filters, filter_sizes, output_dim, dropout rate, padding_index
        model = CNN(vocab_size, 100, 100, [3, 4, 5], 1, 0.5, padd_idx)
        model.to(device)

        #Pass pre-trained vectors
        if(vector_idx != 0):
            pretraining = headline.vocab.vectors
            model.embedding.weight.data.copy_(pretraining)

            #the pre-trained vectors have an embed dimension of 100
            model.embedding.weight.data[headline.vocab.stoi[headline.unk_token]] = torch.zeros(100)
            model.embedding.weight.data[headline.vocab.stoi[headline.pad_token]] = torch.zeros(100)
        
        loss_function = nn.BCEWithLogitsLoss()

        if(optim_idx == 0):
            optimizer = optim.Adam(model.parameters(), lr=learning_rate) #LR = 0.001
        elif(optim_idx == 1):
            optimizer = optim.Adadelta(model.parameters(), lr=learning_rate, rho=0.95) #LR = 0.25
        elif(optim_idx == 2):
            optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9) #LR = 0.01
        

        #Train Network
        print('Training...')
        loss_per_epoch = []
        train_start_time = time.time()

        for i in range(epochs):
            model.train()
            
            train_loss = 0
            for batch in train_iterator:
                batch.h, batch.s = batch.h.to(device), batch.s.to(device)

                optimizer.zero_grad()

                predictions = model(batch.h).squeeze(1)
                loss = loss_function(predictions, batch.s)

                loss.backward()
                optimizer.step()
                
                train_loss += loss.item()

            epoch_loss = train_loss/len(train_iterator)
            loss_per_epoch.append(epoch_loss)
            print('[Epoch {}] loss: {}'.format(i + 1, epoch_loss))
        
        loss_per_run.append(loss_per_epoch)
        
        train_end_time = time.time()
        train_time = round(train_end_time - train_start_time)
        total_train_time += train_time

        training_timestamp = str(datetime.timedelta(seconds=train_time))
        print('Training Time: {}'.format(training_timestamp))


        #Test Network
        print('\nTesting...\n')

        model.eval()

        total_acc = 0
        count = 0    
        for batch in test_iterator:
            batch.h, batch.s = batch.h.to(device), batch.s.to(device)
            
            with torch.no_grad():
                predictions = model(batch.h).squeeze(1)
                    
            acc = get_accuracy(predictions, batch.s)
            total_acc += acc.item()

            # print('Test {} [Acc {}]'.format(count, total_acc / len(test_iterator)))

            count+=1
 
        overall_acc = round(total_acc / len(test_iterator) * 100, 2)
        print('Overall Accuracy: {}'.format(overall_acc))

        acc_per_run.append(total_acc / len(test_iterator))
    
    #log results
    with open('results.csv', 'w+', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Runs:', num_runs])
        writer.writerow(['Epochs:', epochs])
        writer.writerow([])

        writer.writerow(['accuracy per run:'])
        writer.writerow(acc_per_run)
        writer.writerow([])

        acc_per_run = np.array(acc_per_run)
        mean_acc = np.mean(acc_per_run)
        mean_acc = round(mean_acc * 100, 2)


        writer.writerow(['mean accuracy:', mean_acc])
        writer.writerow([])

        mean_train_time = round(total_train_time / num_runs)
        training_timestamp = str(datetime.timedelta(seconds=mean_train_time))

        writer.writerow(['Ave train time:', training_timestamp])
        writer.writerow([])
        
        writer.writerow(['loss over each run'])
        for row in loss_per_run:
            writer.writerow(row)


#gather model params and start the main loop
VECTOR_LIST = ['None', 'Char N-Gram', 'Glove', 'Glove Twitter']
OPTIM_LIST = ['Adam', 'Adadelta', 'SGD']

main_loop = True
while(main_loop):
    use_defaults = input('Use default model parameters? (y/n)\n')

    if(use_defaults == 'n'):
        RUNS = int(input('Enter # of runs: (recommended: 30)\n'))
        EPOCHS = int(input('\nEnter # of training epochs: (recommended: 10)\n'))
        TRAIN_TEST_SPLIT = float(input('\nEnter train/test split: (recommended: 0.7)\n'))
        VECTOR_IDX = int(input('\nEnter a number to use pre-trained vectors (0, 1, 2 or 3):\n0: No vectors\n1: Char N-Gram\n2: Glove\n3: Glove Twitter\n'))
        OPTIM_IDX = int(input('\nEnter a number for the CNN optimizer (0, 1, or 2):\n0: Adam\n1: Adadelta\n2: SGD\n'))
        LR = float(input('\nEnter the learning rate: (recommended: 0.001 for Adam)\n'))
    else:
        RUNS = 30
        EPOCHS = 10
        LR = 0.001
        TRAIN_TEST_SPLIT = 0.7
        VECTOR_IDX = 0
        OPTIM_IDX = 0
    
    print('\nModel Parameters:')
    print('Runs: {}, Epochs: {}, LR: {}, Train/Test Split: {}, Vectors: {}, Optimizer: {}'.format(RUNS, EPOCHS, LR, TRAIN_TEST_SPLIT, VECTOR_LIST[VECTOR_IDX], OPTIM_LIST[OPTIM_IDX]))
    main(RUNS, EPOCHS, LR, TRAIN_TEST_SPLIT, VECTOR_IDX, OPTIM_IDX)
    
    labels = ('Non-Sarcastic', 'Sarcastic')
    title = "Model Correctness"
    display_contigency(actual_tensors,predicted_tensors,labels,title)
        

    again = input("\nRun the model again? (y/n)\n")
    if(again == 'n'):
        main_loop = False

"""
RUNS = 30
EPOCHS = 10
LR = 0.001
TRAIN_TEST_SPLIT = 0.7
VECTOR_IDX = 0
OPTIM_IDX = 0
main(RUNS, EPOCHS, LR, TRAIN_TEST_SPLIT, VECTOR_IDX, OPTIM_IDX)
"""
