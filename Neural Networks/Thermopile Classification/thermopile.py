import sys,os
import torch 
import torch.autograd as grad
import torch.nn as net
import torch.nn.functional as func
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import numpy as np 
import csv
import matplotlib.pyplot as plt
import random
import math
import itertools

from datetime import datetime
from pathlib import Path
from torch.utils.data.dataset import Dataset, random_split
from torch import optim
from PIL import Image
from sklearn.metrics import confusion_matrix, multilabel_confusion_matrix
from sklearn.metrics import plot_confusion_matrix

"""
This course work uses pytorch libraries running on a CentOS server 
thats rocking a ground-breaking CUDA device: the GTX 2060   
"""


"""
Globals
"""
classes = ('1p1ft','1p1fto','1p3ft','1p3fto','1p6ft','2p3ft', '3p3ft', 'nothingi','nothingo')

in_nodes = 8*8
hd_nodes = 128
ot_nodes = len(classes)
batch_size = 4

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
transformations = transforms.Compose([transforms.ToTensor()])


"""
Custom Dataset
"""
class Thermopile_Dataset(Dataset):
    def __init__(self, csv_path, transforms=None):
        xy = np.loadtxt(csv_path, delimiter=",", dtype=np.float32)
        self.x = torch.from_numpy(xy[:, 1:])
        self.y = torch.from_numpy(xy[:, [0]])
        self.length = xy.shape[0]
        self.transforms = transforms

    def __getitem__(self, index):
        img_label = self.y[index]
        img_np = np.asarray(self.x[index]).reshape(8,8).astype('float')
        img_img = Image.fromarray(img_np)
        img_img = img_img.convert('L') #Grayscale

        if self.transforms is not None:
            img_tensor = self.transforms(img_img)
        
        
        return (img_tensor, img_label.long()) #Long for NLL, CrossEntropy


    def __len__(self):
        return self.length


#Only for a sanity check across tests. randomized data usedduring actual testing phase 
def collect_static_data(test):
    if (test == 1): #Presence Indoor Test
        classes = ('1p1ft', 'nothingi')
        train_1p1ft_nothingI = Thermopile_Dataset("train/1p1ft_nothingi_presence_train.csv", transformations)
        test_1p1ft_nothingI = Thermopile_Dataset("test/1p1ft_nothingi_presence_test.csv", transformations)
        return(train_1p1ft_nothingI, test_1p1ft_nothingI, classes)

    elif (test == 2): #Distance Indoor Test
        classes = ('1p1ft', '1p3ft', '1p6ft')
        train_1pnft_distance = Thermopile_Dataset("train/1pnft_distance_train.csv", transformations)
        test_1pnft_distance = Thermopile_Dataset("test/1p1nt_distance_test.csv", transformations)
        return(train_1pnft_distance, test_1pnft_distance, classes)

    elif (test == 3): #Number of Subjects Indoor Testing
        classes = ('1p3ft', '2p3ft', '3p3ft', 'nothingI')
        train_np3ft_presence = Thermopile_Dataset("train/np3ft_subjects_train.csv", transformations)
        test_np3ft_presence = Thermopile_Dataset("test/np3ft_subjects_test.csv", transformations)   
        return(train_np3ft_presence, test_np3ft_presence, classes)

    else: #Distance Outdoor Test
        classes = ('1p1fto', '1p3fto')
        train_np3fto_presence = Thermopile_Dataset("train/1pnfto_distance_train.csv", transformations)
        test_np3fto_presence = Thermopile_Dataset("test/1pnfto_distance_test.csv", transformations)   
        return(train_np3fto_presence, test_np3fto_presence, classes)




def collect_random_data(test):
    if (test == 1): #Presence Indoor Test
        dataset = Thermopile_Dataset("datasets/1p1ft_nothingi_presence.csv", transformations)
        classes = ('1p1ft', 'nothingi')

    elif (test == 2): #Distance Indoor Test
        dataset = Thermopile_Dataset("datasets/1pnft_distance.csv", transformations)
        classes = ('1p1ft', '1p3ft', '1p6ft')

    elif (test == 3): #Number of Subjects Indoor Testing
        dataset = Thermopile_Dataset("datasets/np3ft_subjects.csv", transformations)
        classes = ('1p3ft', '2p3ft', '3p3ft', 'nothingI')

    elif (test == 4): #Distance Outdoor Test
        dataset = Thermopile_Dataset("datasets/1pnfto_distance.csv", transformations)
        classes = ('1p1fto', '1p3fto')

    else:
        dataset = Thermopile_Dataset("data.csv", transformations)
        classes = ('1p1ft','1p1fto','1p3ft','1p3fto','1p6ft','2p3ft', '3p3ft', 'nothingi','nothingo')



    diff = len(dataset) - math.floor(len(dataset)*0.7 + len(dataset)*0.3)
    train_split = int(len(dataset)*0.7)
    test_split = int(len(dataset)*0.3)+1

    train_dataset, test_dataset = random_split(dataset, [train_split, test_split])
    return(train_dataset, test_dataset, classes)



"""
Neural Net Construction
"""
class Net(net.Module):
    def __init__(self, in_nodes, hd_nodes, ot_nodes):
        #Costructor
        super(Net, self).__init__()
        self.input_layer = net.Linear(in_nodes, hd_nodes)
        self.hidden_layer = net.ReLU()
        self.output_layer = net.Linear(hd_nodes, ot_nodes)
        

    def forward(self, x):
        out = self.input_layer(x)
        out = self.hidden_layer(out) 
        out = self.output_layer(out) 
        return out 


def display_sample(train_loader):
    dataiter = iter(train_loader)
    images, labels = dataiter.next()

    plt.imshow(
        np.transpose(torchvision.utils.make_grid(images), (1, 2, 0)), 
        #cmap='gray', 
        vmin=0, 
        vmax=255)

    print(labels)
    plt.show()


def display_contigency(truth, p, labels, title):
    contigency = confusion_matrix(truth, p, labels)

    plt.imshow(contigency, interpolation='nearest', cmap=plt.cm.Greens)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(labels))
    plt.xticks(tick_marks, labels, rotation=45)
    plt.yticks(tick_marks, labels)

    arr = np.asarray(contigency)
    fmt = '.2f'
    thresh = contigency / 2

    for i in range(contigency.shape[0]):
        for j in range(contigency.shape[1]):

            if (contigency[i][j] < thresh).any():
                color = "black"
            else:
                color = "white"

            plt.text(j, i, format(int(contigency[i][j]), fmt), color=color, horizontalalignment="center")

    plt.tight_layout()
    plt.ylabel('Truth')
    plt.xlabel('Predicted')

    plt.show()



"""
Program Logic
"""
def main():

    #Random Seed
    seed_value = random.randrange(sys.maxsize)
    random.seed(seed_value)
    #print(seed_value)

    #Data Collection
    #train_dataset, test_dataset,classes = collect_static_data(3)
    train_dataset, test_dataset,classes = collect_random_data(1)

    thermopile_train_loader = torch.utils.data.DataLoader(
                                                    dataset=train_dataset,
                                                    batch_size=1,
                                                    shuffle=True,
                                                    drop_last=False)
    
    thermopile_test_loader = torch.utils.data.DataLoader(
                                                    dataset=test_dataset,
                                                    batch_size=1,
                                                    shuffle=True,
                                                    drop_last=False) 

    display_sample(thermopile_test_loader)


    #Neural Net Instantiation
    model = Net(in_nodes, hd_nodes, ot_nodes).to(device)
    loss_function = net.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
    #optimizer = optim.Adam(model.parameters(), lr=0.001)

    
    #Train
    with torch.enable_grad():
        for epoch in range(25):
            loss = 0
            for i, (inputs, label) in enumerate(thermopile_train_loader):
                
                #inputs = inputs[0].reshape(-1,8*8).to(device)
                inputs = inputs.view(inputs.shape[0],-1)
                label = label[0].to(device)

                outputs = model(inputs)
                loss = loss_function(outputs, label)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                loss += loss.item()
            
            #print('[Epoch %d] loss: %.10f' % (epoch + 1, loss/len(thermopile_train_loader)))
            #print(('E{}, %.10f,' % (loss/len(thermopile_train_loader))).format(epoch))
                

    #Test
    model.eval() 
    correct = 0
    total = 0

    truth = [] 
    p = [] 

    with torch.no_grad():
        for i, (inputs, label) in enumerate(thermopile_test_loader):
            inputs = inputs[0].reshape(-1,8*8).to(device)
            label = label[0].to(device)

            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, 1)

            total += label.size(0)
            correct += (predicted == label).sum().item()
            preds = np.squeeze(predicted.numpy())
            
            truth.append(classes[label[0].item()]) 
            p.append(classes[preds])

            #print ("Actual: {} \nPredicated: {}\n".format(classes[label[0].item()], classes[preds]))
            #print ("{},{}\n".format(classes[label[0].item()], classes[preds]))

    print('A, %0.6f,' % (100 * correct / total))
    
    return (truth, p)


"""
Program Driver
"""
def driver():
    truths = [] 
    predictions = [] 

    for i in range(30):
        t_tmp, p_tmp = main()

        for i in t_tmp:
            truths.append(i)
        for j in p_tmp:
            predictions.append(j)

        #print(i) #Prove that seed is working, rotating labels* 
   
    labels = ('1p3ft', '2p3ft', '3p3ft', 'nothingI')
    title = "All Data"

    display_contigency(truths, predictions, labels, title)



driver() 