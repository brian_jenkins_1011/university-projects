import os
import sys
import math
import numpy as np
import matplotlib.image as img
import matplotlib.pyplot as plt
import random
import time
import struct
import tkinter as tk
from array import array
from tkinter import filedialog
from PIL import Image
from sys import argv

epoch = 2500
alpha = 0.1
s_neighbourhood = 16
c_alpha = 0.7
filename = ""

"""
SOM Class
"""
class SOM:
    def __init__(self, rows, cols, vec_size, image_array):
        self.rows = rows
        self.cols = cols
        self.vec_size = vec_size
        self.map_vectors = np.random.rand(rows*cols, vec_size,  vec_size)
        self.image_vectors = self.get_image_vectors(image_array)

    
    def get_image_vectors(self, image_array):
        image_array = image_array.reshape(len(image_array)//self.vec_size, self.vec_size, -1, self.vec_size).swapaxes(1,2).reshape(-1, self.vec_size, self.vec_size)
        return image_array


    def get_neighbourhood_radius(self, i):
        max_radius = s_neighbourhood
        time = epoch / math.log(max_radius)
        return max_radius * math.exp(-i / time)


    def get_best_match(self, image_vec):
        best_unit = 0
        best_dist = math.inf

        for i in range(len(self.map_vectors)):
                dist = np.absolute(np.linalg.norm(image_vec) - np.linalg.norm(self.map_vectors[i]))
                if dist < best_dist:
                    best_dist = dist
                    best_unit = i

        row = int(best_unit / self.rows)
        cols = int(best_unit % self.cols)

        return np.array([row,cols])


"""
Useful functions
"""
def rgb_to_byte(image):
    im = np.asarray(image, dtype=np.float32)
    arr = np.zeros((len(im), len(im)))

    for i in range(len(im)):
        for j in range(len(im)):
            arr[i][j] = im[i][j] / 255

    return np.asarray(arr)


def collect_codebook(som_w, som_h, vec_size, SOM):
    map_blocks = SOM.map_vectors.reshape(som_w, som_w, vec_size, vec_size).reshape(som_w**2, vec_size, vec_size)
    stack = map_blocks.reshape(som_w**2, vec_size, vec_size)
    codebook = stack.reshape(som_w*vec_size//stack.shape[1], -1, stack.shape[1], stack.shape[2]).swapaxes(1,2).reshape(som_w*vec_size, som_w*vec_size)
    return codebook





"""
All the hard work is done in here!
"""
def __main__(*args):
    np.random.seed(None)

    global alpha
    global epoch
    global s_neighbourhood
    global c_alpha 
    global filename

    filename = str(sys.argv[1])

    epoch = int(input("How many epochs (rec. ~2500): "))
    som_size = int(input("Map Size (4 or 16): "))
    vec_size = int(input("Vector frame size (8 or 16): "))
    c_alpha = float(input("Starting learning rate (rec. ~0.7): "))
    alpha = c_alpha
    s_neighbourhood = float(input("Select a neighbourhood radius (rec ~5): "))

    som_w = som_size
    som_h = som_size
    vec_size = 8

    mansion = Image.open(filename)
    grey_mansion = mansion.convert('L')
    mansion_arr = rgb_to_byte(grey_mansion)

    plt.imshow(mansion_arr, cmap="gray")
    plt.show()

    s = SOM(som_w, som_h, vec_size, mansion_arr) #SOM Rows, SOM Cols. Vector Dimension, Gray Array

    print("Training SOM...")
    for i in range(epoch):
        rand_vec = int(random.uniform(0,len(s.image_vectors)-1))
        bmu = s.get_best_match(s.image_vectors[rand_vec])

        #Calculate Weight Deltas
        weight_factors = []
        for j in range(s.rows):
            for k in range(s.cols):
                dist = np.linalg.norm(bmu - np.array([j, k])) #Euclidian Distance of 2 (x,y) coords 
                hood = s.get_neighbourhood_radius(i+1)**2 #Neighbourhood Decay
                wf = math.exp(-(dist) / (2*hood)) #Weight Factor
                weight_factors = np.append(weight_factors, alpha * wf) 

        
        #Apply weight factor changes on individual pixels
        for mp_v in range(len(s.map_vectors)):
            pixel_values = []
            for val in (s.image_vectors[rand_vec] - s.map_vectors[mp_v]): #Begin by calculating difference
                pixel_values.append(val * weight_factors[mp_v]) #Multiply new pixel value by the weight factor

            weight = s.map_vectors[mp_v] + np.array(pixel_values) #Add new pixel values to map_vectors to obtain new vector values
            s.map_vectors[mp_v] = weight #Change old pixel values to new pixel values
    
    alpha = c_alpha * math.exp(-i/epoch) #Update Alpha
    

    print("Collecting codebook...")
    codebook = collect_codebook(som_w, som_h, vec_size, s)
    plt.imshow(codebook, cmap="gray")
    plt.show()
    

    #Match image with codebook
    print("Reconstructing image...")
    new_image_indices = np.zeros(len(s.image_vectors))
    for i in range(len(s.image_vectors)):
        best_dist = math.inf
        best_unit = 0

        for j in range(len(s.map_vectors)):
            dist = np.linalg.norm(s.image_vectors[i] - s.map_vectors[j])
            if dist < best_dist:
                best_dist = dist
                best_unit = j

        new_image_indices = np.append(new_image_indices, best_unit)
        s.image_vectors[i] = s.map_vectors[best_unit]


    #Reshape Image and Display
    n, nrows, ncols = s.image_vectors.shape
    compressed = (s.image_vectors.reshape(512//nrows, -1, nrows, ncols)
               .swapaxes(1,2)
               .reshape(512, 512))

    sum_difference = np.sum(np.abs(np.subtract(mansion_arr,compressed,dtype=np.float))) / mansion_arr.shape[0] #Why use python if you cant use one liners? 
    print("Sum of differences: {}".format(sum_difference))

    plt.imshow(compressed, cmap="gray")
    plt.show()
    

__main__(*argv[1])
