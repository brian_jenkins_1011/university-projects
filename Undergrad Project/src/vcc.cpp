#include "vcc.h"
#include "pack.h"

#include <vector>
#include <cmath> 
#include <algorithm>
#include <map>
#include <iostream>

using namespace VCC; 

int vru_template_i [3] = {8, 1024, 5120}; 
int vru_template_ii [3] = {16, 2048, 10240}; 
int vru_template_iii [3] = {32, 4096, 20480}; 

/* Vehicle (Resources) Functionality */ 
VCC::VEHICLE::VEHICLE (int address){
	this->address = address; 
	this->cpu = rand() % (8-4+1) + 4; //4 - 8 Cores
	this->memory = rand() % (1024-512+1) + 512; // 512 - 1024 MB
	this->storage = rand() % (5120-2560+1) + 2560; //1280 - 5120  MB
}

void VCC::VEHICLE::set_assigned(bool assigned){
	this->assigned = assigned; 
}

//Getters
bool VCC::VEHICLE::is_assigned() {
	return assigned; 
}
double VCC::VEHICLE::get_Cpu() {
	return cpu; 
}
double VCC::VEHICLE::get_Memory() {
	return memory; 
}
double VCC::VEHICLE::get_Storage() {
	return storage; 
}
double VCC::VEHICLE::get_total() {
	return (get_Cpu() + get_Memory() + get_Storage());
}

int VCC::VEHICLE::get_Address(){
	return address; 
}
	

/* VRU Functionality */ 
VCC::VRU::VRU (int type){
	this->vehicles = 0; 
	this->cpu = 0; 
	this->memory = 0; 
	this->storage = 0; 
	this->type = type; 
}

int* VCC::VRU::get_template(){
	int* vru_template; 
	switch (type){
		case 1:
			vru_template = vru_template_i; 
			break; 
		case 2:
			vru_template = vru_template_ii; 
			break; 
		case 3:
			vru_template = vru_template_iii;
			break; 
	}
	return vru_template; 
}


void VCC::VRU::assign_resources(VCC::VEHICLE v){
	vehicles+=1; 
	cpu += v.get_Cpu(); 
	memory += v.get_Memory();
	storage += v.get_Storage();
} 

std::map<int, VCC::VEHICLE> VCC::VRU::get_resources_map(){
	return vehicle_resources;
}

//Getters
int VCC::VRU::get_type(){
	return type;
}
double VCC::VRU::get_Cpu(){
	return cpu; 
}
double VCC::VRU::get_Memory(){
	return memory; 
}
double VCC::VRU::get_Storage(){
	return storage; 
}

int VCC::VRU::get_Vehicles(){
	return vehicles; 
}

/* Helper Functionality */ 
std::map<int, VCC::VEHICLE> VCC::generate_nodes(int num_vehicle){
	std::map <int, VCC::VEHICLE> nodes; 
	int address = 0; //Temp address generation

	for (int i=0; i<num_vehicle; i++){
		VCC::VEHICLE v = VCC::VEHICLE(address); 
		nodes.insert(std::make_pair(address, v));
		address++; 
	}
	return nodes; 
}

