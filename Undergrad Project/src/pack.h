#ifndef PACK_H_ 
#define PACK_H_

#include "vcc.h"

namespace PACK {

	extern std::vector<VCC::VRU> vru_list;   

	//Helpers
	extern bool check_fit(VCC::VRU vru, VCC::VEHICLE v);
	extern float check_fit_best(VCC::VRU vru, VCC::VEHICLE v); 
	extern bool check_full(VCC::VRU); 
	extern void print_VRU_list_vector(std::vector<VCC::VRU> Virtual_Resource_Units); 
	extern void print_VRU_list_map(std::map<int, VCC::VRU> Virtual_Resource_Units); 
	extern std::vector<VCC::VRU> populate_vru(int count_i, int count_ii, int count_iii); 

	//Bin Packing
	extern std::map<int, VCC::VRU> pack_next_fit(std::map<int, VCC::VEHICLE> vehicles, std::array<int,3> vru_distritbution);
	extern std::map<int, VCC::VRU> pack_first_fit(std::map<int, VCC::VEHICLE> Vehicle_Pairs, std::array<int,3> vru_distritbution);
	extern std::map<int, VCC::VRU> pack_best_fit(std::map<int, VCC::VEHICLE> Vehicle_Pairs, std::array<int,3> vru_distritbution); 

	extern std::map<int, VCC::VRU> pack_vru_function_binal (std::map<int, VCC::VEHICLE> vehicles, std::array<int,3> vru_distritbution);

	//Heuristics
	extern std::array<int,3> split_heuristic(double total_CPU, double perc_type_i, double perc_type_ii, double perc_type_iii);  
	
};

#endif 
