#include "vcc.h"
#include "pack.h"

#include <vector>
#include <cmath> 
#include <algorithm>
#include <map>
#include <iostream>
#include <array>

using namespace PACK; 

//Check if vehicle resources fit in designated VRU
bool PACK::check_fit(VCC::VRU vru, VCC::VEHICLE v){
	int* vru_template = vru.get_template();  
	bool fit = false; 

	//For small, medium sized VRUs ensure that they stay within bounds 
	auto check_s_m = [&]() {
		if (
			(vru.get_Cpu() + v.get_Cpu()) < vru_template[0]*2 
			&& (vru.get_Memory() + v.get_Memory()) < vru_template[1]*2 
			&& (vru.get_Storage() + v.get_Storage()) < vru_template[2]*2 
			) {
		fit = true;
		} 
	}; 

	//Large VRUs dont require as much work, just make sure they're full
	auto check_l = [&]() {
	if ((vru.get_Cpu() < vru_template[0]) ||
			(vru.get_Memory() < vru_template[1]) ||
			(vru.get_Storage() < vru_template[2])
		 ) {
			fit = true; 
		}
	}; 

	//Switch go fast
	switch(vru.get_type()){
		case 1:
			check_s_m(); 
			break;
		case 2:
			check_s_m(); 
			break;
		case 3:
			check_l(); 
			break;
	}
	return fit;
} 


//Check if a VRU has passed its capacity guidline
bool PACK::check_full(VCC::VRU vru){
	int* vru_template = vru.get_template();  

	if (vru.get_Cpu() >= vru_template[0]
		&& vru.get_Memory() >= vru_template[1]
		&& vru.get_Storage() >= vru_template[2])
		{		
			return true; 		 
		}
	return false; 
}


float PACK::check_fit_best(VCC::VRU vru, VCC::VEHICLE v){

	/*
	Purpose: To return the % of space available considering equal weight among values.
			 This will help determine which VRU will best accomodate any given vehicle
			 by finding the value closest to 0.

	Considerations: Its important to note that the guidlines of VRU assemply are defined 
				    by vals >= to the floor value. So, having a vehicle have +10 cpu from
				    the floor isnt neccesarily incorrect, in fact - this could be better.
				    However, we should be focused on assembly VRUs with the least number
				    of vehicles possible to ensure stability which is why best-fit 
				    searches for the largest vehicle before every assignment process. 
	 */
	
	int* vru_template = vru.get_template(); 

	float cpu_diff = std::abs(vru_template[0] - (vru.get_Cpu() + v.get_Cpu())); 
	float mem_diff = std::abs(vru_template[1] - (vru.get_Memory()  + v.get_Memory())); 
	float stor_diff = std::abs(vru_template[2] - (vru.get_Storage()  + v.get_Storage()));  

	float cpu_perc = cpu_diff / vru_template[0] * 0.34; 
	float mem_perc = mem_diff / vru_template[1] * 0.34; 
	float stor_perc = stor_diff / vru_template[2] * 0.34; 
	
	return (cpu_perc + mem_perc + stor_perc);
} 



//Print VRU List
void PACK::print_VRU_list_vector(std::vector<VCC::VRU> Virtual_Resource_Units){
	int count = 1; 
	for (std::vector<VCC::VRU>::iterator it = Virtual_Resource_Units.begin() ; it != Virtual_Resource_Units.end(); ++it) {
		std::cout << "VRU " << count << std::endl; 
		std::cout << "	Vehicles: " << it->get_Vehicles() << std::endl; 
		std::cout << "	Type: " << it->get_type() << std::endl; 
		std::cout << "	CPU: " << it->get_Cpu() << std::endl; 
		std::cout << "	MEM: " << it->get_Memory() << std::endl; 
		std::cout << "	STOR: " << it->get_Storage() << "\n" << std::endl; 

		count++; 
	} 
}

void PACK::print_VRU_list_map(std::map<int, VCC::VRU> Virtual_Resource_Units){
	int count = 1; 
	for (std::map<int, VCC::VRU>::iterator it = Virtual_Resource_Units.begin() ; it != Virtual_Resource_Units.end(); ++it) {
		std::cout << "VRU " << count << std::endl; 
		std::cout << "	Vehicles: " << it->second.get_Vehicles() << std::endl; 
		std::cout << "	Type: " << it->second.get_type() << std::endl; 
		std::cout << "	CPU: " << it->second.get_Cpu() << std::endl; 
		std::cout << "	MEM: " << it->second.get_Memory() << std::endl; 
		std::cout << "	STOR: " << it->second.get_Storage() << "\n" << std::endl; 

		count++; 
	} 
}


/*
	Best fit bin packing (wo searching)

	Note: VRU dist divided by cpu distrubtuion across VRU types. Intended use in Binals code.
 */
std::map<int, VCC::VRU> PACK::pack_vru_function_binal(std::map<int, VCC::VEHICLE> vehicles, std::array<int,3> vru_distritbution) {
	std::map<int, VCC::VRU> vru_list; 
	
	int count = 1;
	for (int var = 0; var < vru_distritbution[2]/32;  ++var) {
	    VCC::VRU one_vru(3); // Create a new VRU
	    vru_list.insert(std::make_pair(count, one_vru));
	    count++; 
	}

	for (int var = 0; var < vru_distritbution[1]/16;  ++var) {
	    VCC::VRU one_vru(2);
	    vru_list.insert(std::make_pair(count, one_vru));
	    count++; 
	}

	for (int var = 0; var < vru_distritbution[0]/8;  ++var) {
	    VCC::VRU one_vru(1);
	    vru_list.insert(std::make_pair(count, one_vru));
	    count++; 
	}

	while (vehicles.size() > 0){
		for (std::map<int, VCC::VRU>::iterator it = vru_list.begin() ; it != vru_list.end(); ) {
			for (auto sm_pair = vehicles.begin(); sm_pair != vehicles.end();) {

		        if (it->second.get_type() == 3) {
		           if ((it->second.get_Cpu() < 32) and (it->second.get_Storage() < 20480)) {
		              //it->get_Cpu() += vehicles[sm_pair.first].get_Cpu();
		              //it->get_Cpu() += vehicles[sm_pair.first].get_Storage();
		               it->second.assign_resources(sm_pair->second); 
		               sm_pair = vehicles.erase(sm_pair); 

		           }
		           else {
		               break;
		           }
		        }

		        else if (it->second.get_type() == 2){
		            if ((it->second.get_Cpu() < 16) and (it->second.get_Storage() < 10240)) {
		               //it->get_Cpu() += prtable[sm_pair.first].cpu;
		               //it->get_Storage() += prtable[sm_pair.first].storage;
		               it->second.assign_resources(sm_pair->second); 
		               sm_pair = vehicles.erase(sm_pair); 

		            }
		            else {
		                break;
		            }
		         }

		        else if (it->second.get_type() == 1){
		             if ((it->second.get_Cpu()  < 8) and (it->second.get_Storage() < 5120)) {
	                   //it->get_Cpu()  += prtable[sm_pair.first].cpu;
	                   //it->get_Storage() += prtable[sm_pair.first].storage;
	               		it->second.assign_resources(sm_pair->second); 
	              		sm_pair = vehicles.erase(sm_pair); 

		            }
		            else {
		                break;
		            }
		          }
			}
			 ++it; 
		}
		
		//Fill Remaning Space with small VRUs (in order for all vehicles to be used)
		VCC::VRU one_vru(1); //In all tests only one of these is needed...??!
	    vru_list.insert(std::make_pair(count, one_vru));
	    count++;
	} 

	//NOTE: Many small VRUs will be completley empty since we assemble the VRUs first without checking if they're needed
	//		To keep on par, I remove these before returning the list. Unfair to compare with other algos
	std::map<int, VCC::VRU> completed_vru; 
	for (auto i : vru_list){
		if (i.second.get_Vehicles() > 0){
			completed_vru.insert(std::make_pair(i.first,i.second)); 
		}
	}
	return completed_vru; 
}


/*
 * Next Fit Bin Packing (wo searching)
 */
std::map<int, VCC::VRU> PACK::pack_next_fit(std::map<int, VCC::VEHICLE> Vehicle_Pairs, std::array<int,3> vru_distritbution){
	
	int vru_i = vru_distritbution[0];
	int vru_ii = vru_distritbution[1];
	int vru_iii = vru_distritbution[2]; 

	std::map<int, VCC::VRU> Virtual_Resource_Units; 
	int vru_address = 0; 

	//Assemble in next-fit fashion
	auto assemble_next = [&](int type, int resource_capacity) {
		int count = 0; 
		VCC::VRU vru(type);
		
		while (true) {
			for (auto pair = Vehicle_Pairs.begin(); pair != Vehicle_Pairs.end();) {

				if (check_fit(vru, pair->second)){ //If it fits, add and remove pairing 
					vru.assign_resources(pair->second);
					pair = Vehicle_Pairs.erase(pair); 
					count += pair->second.get_Cpu(); 

					if (Vehicle_Pairs.size() < 1){
						Virtual_Resource_Units.insert(std::make_pair(vru_address, vru));
						goto end; 
					}

					break;  
				}

				else { //If the next item wont fit, push and break
					Virtual_Resource_Units.insert(std::make_pair(vru_address, vru));
					vru_address++; 
					vru = VCC::VRU(type); 
				}

				if (count >= resource_capacity && type !=1){ //fill up remaining resources with small vrus
					goto end; 
				}
			} 	
		}
		end:
		count = 0;  
	}; 

	//Assemble Determined Counts
	assemble_next(3, vru_iii);
	assemble_next(2, vru_ii); 
	assemble_next(1, vru_i);
	

	//Analysis
	//print_VRU_list_map(Virtual_Resource_Units); 
	return Virtual_Resource_Units; 
}


/*
 * First Fit Bin Packing (wo searching)
 */
std::map<int, VCC::VRU> PACK::pack_first_fit(std::map<int, VCC::VEHICLE> Vehicle_Pairs, std::array<int,3> vru_resource_distribution){
	int vru_i = vru_resource_distribution[0];
	int vru_ii = vru_resource_distribution[1];
	int vru_iii = vru_resource_distribution[2]; 
	int vru_address = 0; 

	std::map<int, VCC::VRU> Virtual_Resource_Units; 
	std::map<int, VCC::VRU> Virtual_Resource_Units_Complete; 

   	std::map<int, VCC::VRU>::iterator vru_pair;
   	std::map<int, VCC::VEHICLE>::iterator vehicle_pair;

   	//Pack in a first-fit manner
	auto assemble_first = [&](int type, int vru_resources){
		
		int count = 0; 
		bool vehicle_is_assigned;
		VCC::VRU tmp(type); 
		int* vru_template = tmp.get_template(); 


		while (true){
			vehicle_is_assigned = false;

			for (vehicle_pair = Vehicle_Pairs.begin(); vehicle_pair != Vehicle_Pairs.end(); ++vehicle_pair) { //Iterate Vehicle
				for (vru_pair = Virtual_Resource_Units.begin(); vru_pair != Virtual_Resource_Units.end(); ++vru_pair) { //Iterate VRUs

					if (check_fit(vru_pair->second, vehicle_pair->second)){ //Check for a bin
						vru_pair->second.assign_resources(vehicle_pair->second); 
						vehicle_is_assigned = true; 
						count += vehicle_pair->second.get_Cpu(); 
						vehicle_pair = Vehicle_Pairs.erase(vehicle_pair); 

						if (Vehicle_Pairs.size() < 1){
							goto end;
						}

						else if (check_full(vru_pair->second)){
							Virtual_Resource_Units_Complete.insert(std::make_pair(Virtual_Resource_Units_Complete.size(), vru_pair->second)); 
							vru_pair = Virtual_Resource_Units.erase(vru_pair);
						}
						break; 
					}
				}

				//Doesnt fit? Make a new one!
				if (vehicle_is_assigned == false){
					VCC::VRU new_vru(type);
					
					new_vru.assign_resources(vehicle_pair->second); 
					Virtual_Resource_Units.insert(std::make_pair(vru_address,new_vru));

					count += vehicle_pair->second.get_Cpu(); 
					vru_address++;
					
					vehicle_pair = Vehicle_Pairs.erase(vehicle_pair); 
					if (Vehicle_Pairs.size() < 1){
							goto end;
					}
				} 

				//Exceed Medium and Large Limut
				if (count >= vru_resources && type != 1){
					goto end; 
				}
			} 
		}
		end:
		count = 0;  
	};  

	//Assemble Determined Counts
	assemble_first(3, vru_iii);
	assemble_first(2, vru_ii); 
	assemble_first(1, vru_i); 

	//print_VRU_list_map(Virtual_Resource_Units); 
	Virtual_Resource_Units_Complete.insert(Virtual_Resource_Units.begin(), Virtual_Resource_Units.end()); //Add leftover (incomplete) vrus to list for measuring
	return Virtual_Resource_Units_Complete; 
}


/*
 * Best Fit Packing (w searching)
 */
std::map<int, VCC::VRU> PACK::pack_best_fit(std::map<int, VCC::VEHICLE> Vehicle_Pairs, std::array<int,3> vru_resource_distribution){
	int vru_i = vru_resource_distribution[0];
	int vru_ii = vru_resource_distribution[1];
	int vru_iii = vru_resource_distribution[2]; 
		
	std::map<int, VCC::VRU> Virtual_Resource_Units; 
	std::map<int, VCC::VRU> Virtual_Resource_Units_Complete; 
   	
   	std::map<int, VCC::VRU>::iterator vru_pair;
   	std::map<int, VCC::VEHICLE>::iterator vehicle_pair;


	int vru_address = 0; //Only used  for indexing in map
	auto assemble_best = [&](int type, int vru_resources){
		int count = 0; 
		VCC::VRU tmp(type); 
		int* vru_template = tmp.get_template();

		/*
			Create Initial (empty) VRUs
			To get the ball rolling, and to make sure we're actually solving a bin packing problem, we need some bins to play with.
			Otherwise, if we only work with one bin at a time, we are solving a knapsack problem which makes the first-fit and
			best-fit implementations more-or-less the same. 
		 */
		int num_vru_estimate = std::floor((vru_resources / vru_template[0]) / 4); //Playing nice with numbers again.

		for (int i=0; i<num_vru_estimate; i++){
			VCC::VRU new_vru(type);
			Virtual_Resource_Units.insert(std::make_pair(vru_address, new_vru)); 
			vru_address++; 
		}

		//Start packing!
		while (Vehicle_Pairs.size() > 0){
			
			/*
				Searching
				Here, we're going to sum all avail resources of a vehicle and match against the others in the map to find the largest
			*/
			int max = -1, largest_vehicle_address = -1; 

			for (vehicle_pair = Vehicle_Pairs.begin(); vehicle_pair != Vehicle_Pairs.end(); ++vehicle_pair) {
				int resource_dist = std::floor((vehicle_pair->second.get_Cpu()) + (vehicle_pair->second.get_Memory()) + vehicle_pair->second.get_Storage()); 
				
				if (resource_dist > max){
					largest_vehicle_address = vehicle_pair->second.get_Address();
					max = resource_dist; 
				}
			} 	
			vehicle_pair = Vehicle_Pairs.find(largest_vehicle_address); 
			
			
			/*
				Best-Fit Algorithm
				Iterate throiugh all existing VRUs to find a match. Use best_vru_address as an index for the 'best-fit' VRU
			 */
			
			float best_vru_diff = 1; 
			int best_vru_address = -1; 

			for (vru_pair = Virtual_Resource_Units.begin(); vru_pair != Virtual_Resource_Units.end(); ++vru_pair){
				float total_diff = check_fit_best(vru_pair->second, vehicle_pair->second); 
				
				if (total_diff < best_vru_diff){ //If the current vehicle fits better than the previous
					best_vru_diff = total_diff; 
					best_vru_address = vru_pair->first; 
					//std::cout << best_vru_address << std::endl; 
				}
			}
			
			/*
				VRU Creation
				If the best_vru_address is left unchanched, the vehicle didnt fit in any available VRUs so, create a new one
				Otherwise, append to the best_vru_address then check if the VRU in question is full.
			 */
			if (best_vru_address == -1){
				VCC::VRU new_vru(type);
				new_vru.assign_resources(vehicle_pair->second); 
				Virtual_Resource_Units.insert(std::make_pair(vru_address, new_vru)); 

				count += vehicle_pair->second.get_Cpu(); 
				vru_address++; 
				vehicle_pair = Vehicle_Pairs.erase(vehicle_pair);
				
				if (count >= vru_resources && type != 1){
					goto end; 
				}
			}
			else {
				vru_pair =  Virtual_Resource_Units.find(best_vru_address); 

				vru_pair->second.assign_resources(vehicle_pair->second);
				count += vehicle_pair->second.get_Cpu(); 

				if (check_full(vru_pair->second)){
					Virtual_Resource_Units_Complete.insert(std::make_pair(Virtual_Resource_Units_Complete.size(), vru_pair->second)); 
					vru_pair = Virtual_Resource_Units.erase(vru_pair);
				}
				vehicle_pair = Vehicle_Pairs.erase(vehicle_pair);
			}
		}
		end:
		count = 0; 
	};  

	//Assemble Determined Counts
	assemble_best(3, vru_iii);
	assemble_best(2, vru_ii); 
	assemble_best(1, vru_i); 

	Virtual_Resource_Units_Complete.insert(Virtual_Resource_Units.begin(), Virtual_Resource_Units.end()); //Add left over (incomplete) vrus to list for measuring
	return Virtual_Resource_Units_Complete; 
}



/* Split Heuristic - Robson Suggestion 

   Looking at a 50/25/25 resource dist with approx 50/30/20 VRU dist (sml, med, lrg)
*/ 
std::array<int,3> PACK::split_heuristic(double total_CPU, double perc_type_i, double perc_type_ii, double perc_type_iii) {
	
	/*
	//Based off car resources
	double cpu_distribution = 8*(perc_type_iii/100) + 5.34*(perc_type_ii/100) + 2.67*(perc_type_i/100); 
	double car_esitmate = std::ceil(total_CPU / cpu_distribution); //Static cpu, mem, stor
	
	//Simplifies to t*p 
	int vru_i = std::floor(((total_CPU * (cpu_distribution*(perc_type_i/100))) / cpu_distribution) / 5); 
	int vru_ii = std::floor(((total_CPU * (cpu_distribution*(perc_type_ii/100))) / cpu_distribution) / 15); 
	int vru_iii = std::floor(((total_CPU * (cpu_distribution*(perc_type_iii/100))) / cpu_distribution) / 30);

	std::cout << "Type Count = {" << vru_i<< ", " << vru_ii << ", " << vru_iii << "}" << std::endl; 
	std::array<int,3> dist = {vru_i,vru_ii,vru_iii}; 
	*/ 


	
	//Based of VRU definitions (Robson suggestion)
	double cpu_distribution = 32*(perc_type_iii/100) + 16*(perc_type_ii/100) + 8*(perc_type_i/100); //25,25,50
	
	double car_esitmate = std::ceil(total_CPU / cpu_distribution) * 3; //Static cpu, mem, stor

	//Reverse perc_type_i and perc_type_iii to allow for more resources in large vrus compared to small (depc)
	//Simplified to t(p+1) / 4 
	int resources_iii= std::floor(total_CPU * (cpu_distribution*(1+(perc_type_iii/100)) / (cpu_distribution*4))); 
	int resources_ii = std::floor(total_CPU * (cpu_distribution*(1+(perc_type_ii/100)) / (cpu_distribution*4)));  
	int resources_i = std::floor(total_CPU * (cpu_distribution*(1+(perc_type_i/100)) / (cpu_distribution*4))); 
	
	
	//std::cout << car_esitmate << std::endl; //split heuristic

	//Some Prints: 
	// std::cout << "Car Estimate: " << car_esitmate << std::endl; 
	// std::cout << "Total CPU: " << total_CPU << std::endl; 
	// std::cout << "Resource Count = {" << resources_i << ", " << resources_ii << ", " << resources_iii << "}" << "->" <<resources_i+resources_ii+resources_iii << std::endl; 
	// std::cout << "Type Count = {" << resources_i / 4 << ", " << resources_ii / 16 << ", " << resources_iii / 32 << "}" << std::endl; 
	// std::cout << "Dist: " << (total_CPU * (cpu_distribution*(perc_type_i/100))) << std::endl;
	

	//std::array<int,3> dist = {resources_i/8,resources_ii/16,resources_iii/32}; 
	std::array<int,3> dist = {resources_i, resources_ii, resources_iii}; 

	
	return dist; 
}
