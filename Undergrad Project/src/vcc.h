#ifndef VCC_H_ 
#define VCC_H_

#include <vector>
#include <cmath> 
#include <algorithm>
#include <map>


namespace VCC { 

	struct VEHICLE {
		int address; 
		double cpu,memory,storage;
		bool assigned; 

		VEHICLE(int address); 

		void set_assigned(bool a); 
		void set_address(int address); 

		bool is_assigned();

		double get_Cpu();
		double get_Memory();
		double get_Storage();
		double get_total();

		int get_Address(); 
		int get_Vehicles(); 
	};

	struct VRU {
		int id,type, vehicles; 
		double cpu,memory,storage;
		std::map<int, VEHICLE> vehicle_resources; 
		
		VRU (int type); 

		int* get_template(); 
		int* get_next_template(); 
		void assign_resources(VEHICLE vehicle);
		std::map<int, VEHICLE> get_resources_map();
		
		void set_type (int type){}; 

		int get_type();
		double get_Cpu();
		double get_Memory();
		double get_Storage();
		int get_Vehicles(); 
	}; 
	
	extern std::map<int, VCC::VEHICLE> generate_nodes(int num_vehicle); 
	extern std::array <int, 3> dist; 
}; 

#endif