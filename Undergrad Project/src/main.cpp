#include "vcc.h" 
#include "pack.h"

#include <iostream> 
#include <vector>  
#include <cmath> 
#include <algorithm>
#include <ctime> 
#include <cstdlib> 
#include <time.h>
#include <map>
#include <ctime>
#include <array> 
#include <cstdio>
#include <stdlib.h>


/* TESTING VARS */ 
std::vector<std::map<int, VCC::VRU>> Assembled_VCC_VRUs;
std::vector<std::vector<double>> VRU_Assembly_Info; 


/* Sorting Methods */ 
bool compare_vehicles(std::pair<int, VCC::VEHICLE> p1, std::pair<int, VCC::VEHICLE> p2){
	return p1.second.get_Cpu() < p2.second.get_Cpu();
}


std::map<int, VCC::VEHICLE> order_vehicles(std::map<int, VCC::VEHICLE> unordered_map){
	std::vector<std::pair<int, VCC::VEHICLE>> order_vehicle; 
	std::map<int, VCC::VEHICLE> order_map;  

	//Order Addresses by Largest CPU 
	for (auto i : unordered_map){
		order_vehicle.push_back(i); 
	} 

	std::sort(order_vehicle.begin(), order_vehicle.end(), compare_vehicles); 

	for (int i=0; i<order_vehicle.size(); i++){
		order_map.insert(std::make_pair(order_vehicle.at(i).first, order_vehicle.at(i).second)); 
	}

	return order_map; 
}


void print_vehicles(std::map<int, VCC::VEHICLE> unordered_map){
	for (auto i : unordered_map){
		std::cout << i.second.get_Cpu() << " " << i.second.get_Memory() << " " << i.second.get_Storage() << " " << std::endl; 
	} 
}

/* REPORTING METHODS */

//Test Splitting Heuristic Accuracy
void test_split(int vehicle_count){
	std::map<int, VCC::VEHICLE> nodes = VCC::generate_nodes(vehicle_count);

	double cpu = 0; 
	for (auto i : nodes){
		cpu += i.second.get_Cpu(); 
	}

	std::cout << vehicle_count << ", ";  // cont'd vcc::split_heuristic comments | Actual, Estimated
	std::array<int,3> vru_distribution = PACK::split_heuristic(cpu, 50, 25, 25); //S:50% M:25% L:25%
}



//Test Bin Packing Methods (number of vehicles, packing duration)
std::map<int, VCC::VRU> test_bin_packing(int method, std::map<int, VCC::VEHICLE> nodes, int run){
	srand((unsigned int)time(NULL));
	
	double cpu = 0; 
	for (auto i : nodes){
		cpu += i.second.get_Cpu(); 
	}

	/* Distribution Heuristic*/
	//std::cout << "VCC Configuration " << run << std::endl; 
	std::array<int,3> vru_distribution = PACK::split_heuristic(cpu, 50, 25, 25); //S:50% M:25% L:25%

	std::clock_t start;
    double duration;
    start = std::clock();

    //Compute VCC Generation
    std::map<int, VCC::VRU> generation; 
    switch (method){
    	case 1:
    		generation = PACK::pack_next_fit(nodes, vru_distribution);
    		break; 
    	case 2:
    		generation = PACK::pack_first_fit(nodes, vru_distribution); 
    		break; 
    	case 3:
    		generation = PACK::pack_best_fit(nodes, vru_distribution);
    		break; 
    	case 4:
    		generation = PACK::pack_vru_function_binal(nodes, vru_distribution); 
    		break; 
    }

    duration = (std::clock() - start ) / (double) CLOCKS_PER_SEC;
    //std::cout << duration; 
     
    //Get Number of VRU's in Generation
    double vru_i=0, vru_ii=0, vru_iii=0; 
    double s_i=0, s_ii=0, s_iii=0; 

    for (auto i : generation){
    	// vehicles > 0 not required anymore; changes binals list return
    	if (i.second.get_type() == 1 && i.second.get_Vehicles() > 0){
    		s_i += i.second.get_Vehicles(); 
    		vru_i++;
    	}
    	else if (i.second.get_type() == 2 && i.second.get_Vehicles() > 0){
    		s_ii += i.second.get_Vehicles(); 
    		vru_ii++;
    	}
    	else if (i.second.get_type() == 3 && i.second.get_Vehicles() > 0) {
    		s_iii += i.second.get_Vehicles(); 
    		vru_iii++;
    	}
    }


    std::vector<double> run_info = {
    	double(run), duration, double(nodes.size()), //Parameters
    	
    	std::floor(double(vru_distribution[0])/8), std::floor(double(vru_distribution[1])/16), std::floor(double(vru_distribution[2])/32), //VRU Distribution
    	
    	vru_i, vru_ii, vru_iii, //Number of VRUs 
    	
    	s_i/vru_i, s_ii/vru_ii, s_iii/vru_iii //Average Number of Vehicles per VRU size
    }; //Gen, Vehicles, Dur, VRU (heur x3), VRU (act x3), stabality (s_ix3)
    

    VRU_Assembly_Info.push_back(run_info); 
    return generation; 
}


void print_assembly_info(){
	//VRU Assembly Testing
	std::cout << "\n" << std::endl; 
	for (int i=0; i<VRU_Assembly_Info.size(); i++){
		std::cout << 
			VRU_Assembly_Info.at(i).at(0) << ", " << 
			VRU_Assembly_Info.at(i).at(1) << ", " << 
			VRU_Assembly_Info.at(i).at(2) << ", " << 
			VRU_Assembly_Info.at(i).at(3) << ", " << 
			VRU_Assembly_Info.at(i).at(4) << ", " << 
			VRU_Assembly_Info.at(i).at(5) << ", " <<
			VRU_Assembly_Info.at(i).at(6) << ", " <<
			VRU_Assembly_Info.at(i).at(7) << ", " <<
			VRU_Assembly_Info.at(i).at(8) << "," <<
			VRU_Assembly_Info.at(i).at(9) << "," <<
			VRU_Assembly_Info.at(i).at(10) << "," <<
			VRU_Assembly_Info.at(i).at(11) << std::endl; 
	}
}



//Calculate Unused Vehicles/Vehicular Resrouces (finding holes)
void print_resource_usage(std::map<int, VCC::VEHICLE> Vehicles, std::map<int, VCC::VRU> VCC){

	double used_cpu = 0, available_cpu = 0; 
	double used_memory = 0, available_memory = 0; 
	double used_storage = 0, available_storage = 0; 

	int used_vehicles = 0, availale_vehicles = Vehicles.size(); 

	//Available 
	for (auto i : Vehicles){
		 available_cpu += i.second.get_Cpu(); 
		 available_memory += i.second.get_Memory(); 
		 available_storage += i.second.get_Storage(); 
	}

	for (auto i : VCC){
		 used_vehicles += i.second.get_Vehicles(); 
		 used_cpu += i.second.get_Cpu(); 
		 used_memory += i.second.get_Memory(); 
		 used_storage += i.second.get_Storage(); 
	}

	std::cout << ", " << availale_vehicles << ", " << available_cpu << ", " << available_memory << ", " << available_storage << ", " <<
		used_vehicles << ", " << used_cpu << ", " << used_memory << ", " << used_storage; 
}


void print_all_vru(std::map<int, VCC::VRU> VCC){
	double used_cpu = 0;
	double used_memory = 0;
	double used_storage = 0;

	int t_i = 0,t_ii = 0,t_iii = 0; 

	for (auto i : VCC){ //Iterate through VRUs 
		int* tmp = i.second.get_template(); 
		
		used_cpu = i.second.get_Cpu(); 
		used_memory = i.second.get_Memory(); 
		used_storage = i.second.get_Storage(); 
		
		std::cout << "C{" << tmp[0] << "," << used_cpu << "}, M{" 
			<< tmp[1] << "," << used_memory << "}, S{"
			<< tmp[2] << "," << used_storage << "}"
			<< " made with " << i.second.get_Vehicles() 
			<< std::endl; 


		if (i.second.get_type() == 1){
			t_i++; 
		}
		else if (i.second.get_type() == 2){
			t_ii++; 
		}
		else {
			t_iii++; 
		}
	}
	int sum = t_i + t_ii + t_iii; 

	std::cout << t_i << " " << t_ii << " " << t_iii << " " << std::endl; 
	std::cout << float(t_i)/sum << " " << float(t_ii)/sum << " " << float(t_iii)/sum << " " << std::endl; 

}


void print_fit(std::map<int, VCC::VRU> VCC){
	
	std::vector<VCC::VRU> vru_finished; 
	std::vector<VCC::VRU> vru_unfinished; 

	std::vector<VCC::VRU> vru_wo_cpu; 
	std::vector<VCC::VRU> vru_wo_mem; 
	std::vector<VCC::VRU> vru_wo_str; 


	for (auto i : VCC){
		int* tmp = i.second.get_template(); 
		bool unfinished = false; 

		if (i.second.get_Cpu() < tmp[0]){
			vru_wo_cpu.push_back(i.second); 
			unfinished = true; 
		
		} if (i.second.get_Memory() < tmp[1]) {
			vru_wo_mem.push_back(i.second); 
			unfinished = true;

		} if (i.second.get_Storage() < tmp[2]) {
			vru_wo_str.push_back(i.second); 
			unfinished = true;
		}	


		if (unfinished){
			vru_unfinished.push_back(i.second); 
		}
		else {
			vru_finished.push_back(i.second); 
		}
	}

	std::cout << vru_finished.size() << ", " << vru_unfinished.size() << std::endl;
	//std::cout << vru_finished.size() << ", " << vru_unfinished.size() << ", " << vru_wo_cpu.size() << ", " << vru_wo_mem.size() << ", " << vru_wo_str.size() << std::endl; 
} 

//Disgusting technique but it works!
void print_vru_heuristic_distance(std::map<int, VCC::VRU> gen){

	double min_cpu_s = 0; 
	double min_mem_s = 0; 
	double min_stor_s = 0; 

	double min_cpu_m = 0; 
	double min_mem_m = 0; 
	double min_stor_m = 0; 

	double min_cpu_l = 0; 
	double min_mem_l = 0; 
	double min_stor_l = 0; 


	double total_cpu_difference_s = 0; 
	double total_mem_difference_s = 0; 
	double total_stor_difference_s = 0; 

	double total_cpu_difference_m = 0; 
	double total_mem_difference_m = 0; 
	double total_stor_difference_m = 0; 

	double total_cpu_difference_l = 0; 
	double total_mem_difference_l= 0; 
	double total_stor_difference_l = 0; 

	for (auto i : gen){
		int* vru_template = i.second.get_template();  
		
		switch(i.second.get_type()){
			case 1:
			min_cpu_s += vru_template[0]; 
			min_mem_s += vru_template[1]; 
			min_stor_s += vru_template[2]; 


			total_cpu_difference_s += (i.second.get_Cpu() - vru_template[0]);  
			total_mem_difference_s += (i.second.get_Memory() - vru_template[1]);  
			total_stor_difference_s += (i.second.get_Storage() - vru_template[2]);  
			break; 

			case 2:
			min_cpu_m += vru_template[0]; 
			min_mem_m += vru_template[1]; 
			min_stor_m += vru_template[2]; 


			total_cpu_difference_m += (i.second.get_Cpu() - vru_template[0]);  
			total_mem_difference_m += (i.second.get_Memory() - vru_template[1]);  
			total_stor_difference_m += (i.second.get_Storage() - vru_template[2]);  
			break;

			case 3:
			min_cpu_l += vru_template[0]; 
			min_mem_l += vru_template[1]; 
			min_stor_l += vru_template[2]; 


			total_cpu_difference_l += (i.second.get_Cpu() - vru_template[0]);  
			total_mem_difference_l += (i.second.get_Memory() - vru_template[1]);  
			total_stor_difference_l += (i.second.get_Storage() - vru_template[2]);  
			break;  	
		}
	}

	double total_distance_s = ((total_cpu_difference_s+total_mem_difference_s+total_stor_difference_s)/(min_cpu_s+min_mem_s+min_stor_s));
	double total_distance_m = ((total_cpu_difference_m+total_mem_difference_m+total_stor_difference_m)/(min_cpu_m+min_mem_m+min_stor_m));
	double total_distance_l = ((total_cpu_difference_l+total_mem_difference_l+total_stor_difference_l)/(min_cpu_l+min_mem_l+min_stor_l));

	std::cout << total_distance_s << ","
			<< total_distance_m << ","
			<< total_distance_l
	<< std::endl;

	//std::cout << min_cpu+min_mem+min_stor << "," << total_cpu_difference+total_mem_difference+total_stor_difference << std::endl; 
}

//Disgusting technique but it works!
void print_vru_overflow_resources(std::map<int, VCC::VRU> gen){

	double min_cpu = 0; 
	double min_mem = 0; 
	double min_stor = 0; 

	double total_cpu_difference = 0; 
	double total_mem_difference = 0; 
	double total_stor_difference = 0; 


	for (auto i : gen){
		int* vru_template = i.second.get_template();  

		min_cpu += vru_template[0]; 
		min_mem += vru_template[1]; 
		min_stor += vru_template[2]; 

		total_cpu_difference += (i.second.get_Cpu() - vru_template[0]);  
		total_mem_difference += (i.second.get_Memory() - vru_template[1]);  
		total_stor_difference += (i.second.get_Storage() - vru_template[2]);  
	}
	
	double total_distance = (total_cpu_difference+total_mem_difference+total_stor_difference);

	std::cout << total_cpu_difference << "," << total_mem_difference << "," << total_stor_difference << std::endl;

	//std::cout << min_cpu+min_mem+min_stor << "," << total_cpu_difference+total_mem_difference+total_stor_difference << std::endl; 
}


/* Driver */ 
int main() {
	srand((unsigned int)time(NULL));

	
	//Split Testing
	//for (int i=1; i<=50; i++){
	//	test_split(2*(std::pow(i,2))); 
	//}
	
	//Assemble VCC Networks
	for (int i=1; i<=50; i++){

		std::map<int, VCC::VEHICLE> Vehicles = VCC::generate_nodes(25*i);
		//std::map<int, VCC::VEHICLE> Vehicles = VCC::generate_nodes(250*i);
		

		//print_vehicles(Vehicles); 
		//std::map<int, VCC::VEHICLE> ordered = order_vehicles(Vehicles); 

		std::map<int, VCC::VRU> tmp = test_bin_packing(3, Vehicles, i); //Increase number of vehicles for each increment	
		
		//Tests
		//std::cout << i;
		//print_resource_usage(Vehicles, tmp); 
		//std::cout << std::endl; 
		
		//print_fit(tmp); 
		
		print_all_vru(tmp); //Prints VRUs
		
		//std::cout << Vehicles.size() << ","; 
		//print_vru_overflow_resources(tmp); 

		//Assembled_VCC_VRUs.push_back(tmp); 
	}
	//print_assembly_info(); //VRU Stability testing
}